package cl.pall.estacionamiento.estacionamiento.Utils;

/**
 * Creado por Nicolás en 27/09/2018.
 */
public class Constant {
    /////////////////////////
    // VARIABLES GLPOBALES //
    /////////////////////////
    private static String IP = "http://186.10.19.170";

    //PARA TESTING EDUARDI
    //public static String NODO_FIREBASE = "registros";
    public static String NODO_FIREBASE = "automoviles";

    private static String WEB_SERVICE = "/wsEstacionamiento/";
    private static String SVC_VEHICULO = WEB_SERVICE + "Servicios/Vehiculo.svc/rest/";
    private static String SVC_TARIFA = WEB_SERVICE + "Servicios/Tarifa.svc/rest/";

    public static String URL_INGRESO = SVC_VEHICULO + "registro";
    public static String URL_LISTAR = SVC_VEHICULO + "listar_registros";
    public static String URL_SINCRONIZAR = SVC_VEHICULO + "sincronizar";

    public static String URL_CALCULAR_TARIFA = SVC_TARIFA + "calcular_tarifa";
}