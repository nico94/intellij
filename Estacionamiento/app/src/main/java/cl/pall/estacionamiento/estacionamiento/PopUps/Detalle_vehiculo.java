package cl.pall.estacionamiento.estacionamiento.PopUps;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import cl.pall.estacionamiento.estacionamiento.Actividades.Inicio;
import cl.pall.estacionamiento.estacionamiento.Modelos.Transaccion;
import cl.pall.estacionamiento.estacionamiento.R;
import cl.pall.estacionamiento.estacionamiento.Utils.Constant;
import cl.pall.estacionamiento.estacionamiento.WebServices.VolleySingleton;
import com.android.volley.*;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.firebase.database.*;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Hashtable;
import java.util.Map;

public class Detalle_vehiculo extends AppCompatActivity {
    final static String TAG = Detalle_vehiculo.class.getSimpleName();
    private String id = null;
    private FirebaseDatabase database = FirebaseDatabase.getInstance();
    private TextView txt_patente, txt_estado, txt_fecha_inicio, txt_hora_inicio, txt_fecha_salida, txt_hora_salida, txt_tiempo_tras, txt_pago;
    private TextView txt_tarifando;
    private Button btn_de_despacho;
    private LinearLayout lin_det_despachado;
    private ImageButton btn_reimprimir, btn_cerrar;
    // Web Services
    private VolleySingleton volleySingleton;
    private RequestQueue requestQueue;
    private Transaccion transaccion = new Transaccion();

    private boolean tar_estado = false;
    private String tar_mensaje = null;
    private String tar_fecha_entrada = null;
    private String tar_hora_entrada = null;
    private String tar_fecha_salida = null;
    private String tar_hora_salida = null;
    private String tar_id_transaccion = null;
    private String tar_patente = null;
    private int tar_total_cobro = 0;
    private String tar_tiempo_transcurrido = "";
    private String tar_imagen = "";

    private LinearLayout lin_carga, lin_datos;
    private boolean modo_telefono, imprime_ingreso, imprime_despacho, menu_ingreso, menu_despacho, imprime_qr;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalle_vehiculo);
        getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        txt_patente = findViewById(R.id.det_patente);
        txt_estado = findViewById(R.id.det_estado);
        lin_det_despachado = findViewById(R.id.lin_det_despachado);
        lin_carga = findViewById(R.id.linear_carga);
        lin_datos = findViewById(R.id.linear_datos);
        txt_fecha_inicio = findViewById(R.id.det_fecha_inicio);
        txt_hora_inicio = findViewById(R.id.det_hora_inicio);
        txt_fecha_salida = findViewById(R.id.det_fecha_fin);
        txt_hora_salida = findViewById(R.id.det_hora_fin);
        txt_tiempo_tras = findViewById(R.id.det_tiempo_tras);
        txt_pago = findViewById(R.id.det_pago);
        txt_tarifando = findViewById(R.id.txt_tarifando);
        btn_de_despacho = findViewById(R.id.btn_de_despachar);
        btn_reimprimir = findViewById(R.id.btn_reimprimir);
        btn_cerrar = findViewById(R.id.btn_cerrar);
        volleySingleton = VolleySingleton.getInstance(getApplicationContext().getApplicationContext());
        requestQueue = volleySingleton.getmRequestQueue();
        mostrarCarga(true);
        eventos();
        leerConfiguracion();
        Bundle extras = getIntent().getExtras();
        if (extras == null) {
            id = null;
        } else {
            id = extras.getString("id");
            assert id != null;
            buscarDetalle(id);
        }
    }

    private void leerConfiguracion() {
        SharedPreferences pref = getApplicationContext().getSharedPreferences(getString(R.string.PREF), Context.MODE_PRIVATE);
        modo_telefono = Boolean.parseBoolean(pref.getString("modo_telefono", ""));
        imprime_ingreso = Boolean.parseBoolean(pref.getString("imp_ingreso", ""));
        imprime_despacho = Boolean.parseBoolean(pref.getString("imp_despacho", ""));
        menu_ingreso = Boolean.parseBoolean(pref.getString("menu_ingreso", ""));
        menu_despacho = Boolean.parseBoolean(pref.getString("menu_despacho", ""));
        imprime_qr = Boolean.parseBoolean(pref.getString("imp_qr", ""));
        if (menu_despacho) {
            btn_de_despacho.setVisibility(View.GONE);
        } else {
            btn_de_despacho.setVisibility(View.VISIBLE);
        }
    }

    private void mostrarCarga(boolean mostrar) {
        if (mostrar) {
            lin_carga.setVisibility(View.VISIBLE);
            lin_datos.setVisibility(View.GONE);
        } else {
            lin_carga.setVisibility(View.GONE);
            lin_datos.setVisibility(View.VISIBLE);
        }
    }

    private void eventos() {
        btn_de_despacho.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Despachar
                Transaccion transUpd = new Transaccion();
                if (tar_id_transaccion != null && tar_estado) {
                    transUpd.setId(tar_id_transaccion);
                    transUpd.setEstado(1);
                    transUpd.setPatente(tar_patente);
                    transUpd.setFechaIngreso(tar_fecha_entrada);
                    transUpd.setHoraIngreso(tar_hora_entrada);
                    transUpd.setFechaSalida(tar_fecha_salida);
                    transUpd.setHoraSalida(tar_hora_salida);
                    transUpd.setPago(tar_total_cobro);
                    transUpd.setImagen(tar_imagen);
                }
                DatabaseReference referenciaDB = database.getReference().child(Constant.NODO_FIREBASE).child(transUpd.getFechaIngreso().replace("-", "")).child(tar_id_transaccion);
                referenciaDB.setValue(transUpd);
                Inicio inicio = new Inicio();
                if (imprime_despacho) {
                    inicio.imprimirComprobante(transUpd);
                }
                finish();
            }
        });
        btn_reimprimir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (transaccion != null) {
                    Inicio inicio = new Inicio();
                    if (transaccion.getEstado() == 0) {
                        if (imprime_ingreso) {
                            inicio.imprimirTicket(transaccion);
                        } else {
                            Toast.makeText(getApplicationContext(), "Impresión no habiliada", Toast.LENGTH_SHORT).show();
                        }
                    } else if (transaccion.getEstado() == 1) {
                        if (imprime_despacho) {
                            inicio.imprimirComprobante(transaccion);
                        } else {
                            Toast.makeText(getApplicationContext(), "Impresión no habilitada", Toast.LENGTH_SHORT).show();
                        }
                    }
                }
            }
        });
        btn_cerrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void buscarDetalle(String id) {
        if (id.length() == 13) {
            String fecha = id.substring(0, 8);
            Query query = database.getReference().child(Constant.NODO_FIREBASE).child(fecha).child(id);
            query.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    if (dataSnapshot.exists()) {
                        transaccion = dataSnapshot.getValue(Transaccion.class);
                        if (transaccion.getEstado() == 0) {
                            txt_estado.setText("Pendiente");
                            lin_det_despachado.setVisibility(View.GONE);
                            btn_de_despacho.setText("Despachar vehículo");
                            calcularTarifaWS(transaccion);
                        } else if (transaccion.getEstado() == 1) {
                            mostrarCarga(false);
                            lin_det_despachado.setVisibility(View.VISIBLE);
                            txt_estado.setText("Despachado");
                            txt_pago.setText("$ " + transaccion.getPago());
                            btn_de_despacho.setVisibility(View.GONE);
                            //Para calcular Tiempo
                            String tiempo_transcurrido = "";
                            Date ingreso = null;
                            Date actual = null;
                            Date salida = null;
                            String fechaIngreso = transaccion.getFechaIngreso();
                            String horaIngreso = transaccion.getHoraIngreso();
                            Date dat = new Date();
                            String fechaActual = (String) android.text.format.DateFormat.format("dd-MM-yyyy", dat.getTime());
                            String horaActual = (String) android.text.format.DateFormat.format("HH:mm:ss", dat.getTime());
                            long diferencia = 0;
                            SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
                            try {
                                ingreso = format.parse(fechaIngreso + " " + horaIngreso);
                                actual = format.parse(fechaActual + " " + horaActual);
                                salida = format.parse(transaccion.getFechaSalida() + " " + transaccion.getHoraSalida());
                                if (transaccion.getEstado() == 0) {
                                    diferencia = actual.getTime() - ingreso.getTime();
                                } else {
                                    diferencia = salida.getTime() - ingreso.getTime();
                                }
                                long segMili = 1000;
                                long minMili = segMili * 60;
                                long horaMili = minMili * 60;
                                long diasMili = horaMili * 24;
                                long diasTranscurridos = diferencia / diasMili;
                                diferencia = diferencia % diasMili;
                                long horasTranscurridas = diferencia / horaMili;
                                diferencia = diferencia % horaMili;
                                long minutosTranscurridos = diferencia / minMili;
                                diferencia = diferencia % minMili;
                                long segundosTranscurridos = diferencia / segMili;
                                if (diasTranscurridos > 0) {
                                    tiempo_transcurrido = diasTranscurridos + " días ";
                                }
                                if (horasTranscurridas > 0) {
                                    tiempo_transcurrido = tiempo_transcurrido + horasTranscurridas + " horas ";
                                }
                                if (minutosTranscurridos > 0) {
                                    tiempo_transcurrido = tiempo_transcurrido + minutosTranscurridos + " minutos ";
                                }
                                tiempo_transcurrido = tiempo_transcurrido + segundosTranscurridos + " segundos";
                                txt_tiempo_tras.setText(tiempo_transcurrido);
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }

                        }
                        txt_patente.setText(transaccion.getPatente());
                        txt_fecha_inicio.setText(transaccion.getFechaIngreso());
                        txt_hora_inicio.setText(transaccion.getHoraIngreso());
                        txt_fecha_salida.setText(transaccion.getFechaSalida());
                        txt_hora_salida.setText(transaccion.getHoraSalida());
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {
                    Toast.makeText(Detalle_vehiculo.this, "Error al cargar el detalle, reintente", Toast.LENGTH_SHORT).show();
                }
            });
        }
    }

    private void calcularTarifaWS(final Transaccion transaccion) {
        requestQueue = volleySingleton.getmRequestQueue();
        JSONObject dataEnvio = new JSONObject();
        try {
            JSONObject padre = new JSONObject();
            padre.put("id", transaccion.getId());
            padre.put("estado", transaccion.getEstado());
            padre.put("patente", transaccion.getPatente());
            padre.put("fechaIngreso", transaccion.getFechaIngreso());
            padre.put("horaIngreso", transaccion.getHoraIngreso());
            padre.put("imagen", transaccion.getImagen());

            dataEnvio.put("trans", padre);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        SharedPreferences pref = Detalle_vehiculo.this.getSharedPreferences(getString(R.string.PREF), Context.MODE_PRIVATE);
        String ip_server = pref.getString("ip_server", "");
        Log.d(TAG, "IP SERVER: " + ip_server + Constant.URL_CALCULAR_TARIFA);
        if (ip_server.length() > 0) {
            Log.d(TAG, "calcularTarifaWS: " + dataEnvio.toString());
            JsonObjectRequest peticion = new JsonObjectRequest(Request.Method.POST, "http://" + ip_server + Constant.URL_CALCULAR_TARIFA, dataEnvio, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    Log.d(TAG, "onResponse: " + response.toString());
                    JSONObject padre;
                    try {
                        padre = response.getJSONObject("CalcularTarifaResult");
                        tar_estado = padre.optBoolean("estado");
                        tar_mensaje = padre.optString("mensaje");
                        tar_fecha_entrada = padre.optString("fecha_entrada");
                        tar_hora_entrada = padre.optString("hora_entrada");
                        tar_fecha_salida = padre.optString("fecha_salida");
                        tar_hora_salida = padre.optString("hora_salida");
                        tar_id_transaccion = padre.optString("id_transaccion");
                        tar_patente = padre.optString("patente");
                        tar_total_cobro = padre.getInt("total_cobro");
                        tar_tiempo_transcurrido = padre.getString("tiempo_transcurrido");
                        tar_imagen = padre.getString("imagen");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    TextView txt_pago = findViewById(R.id.det_pago);
                    TextView txt_tiempo = findViewById(R.id.det_tiempo_tras);
                    if (tar_estado) {
                        if (tar_mensaje.equals("OK")) {
                            if (menu_despacho) {
                                btn_de_despacho.setVisibility(View.VISIBLE);
                                btn_de_despacho.setText("Despachar vehículo");
                            } else {
                                btn_de_despacho.setVisibility(View.GONE);
                                btn_de_despacho.setText("Despachar vehículo");
                            }
                            txt_pago.append("$ " + Integer.toString(tar_total_cobro));
                            txt_tiempo.append(tar_tiempo_transcurrido);
                            if (transaccion.getImagen() != null) {
                                byte[] decodedString = Base64.decode(transaccion.getImagen(), Base64.DEFAULT);
                                Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                                ImageView imageView = findViewById(R.id.det_imagen);
                                imageView.setImageBitmap(decodedByte);
                            }
                            mostrarCarga(false);
                        }
                    } else {
                        mostrarCarga(false);
                        btn_de_despacho.setVisibility(View.GONE);
                        txt_pago.append("Error al determinar");
                        txt_tiempo.append("Error al determinar");
                        txt_tarifando.append("Error al determinar tarifa");
                        Toast.makeText(Detalle_vehiculo.this, "Error al calcular tarifa", Toast.LENGTH_SHORT).show();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    TextView txt_pago = findViewById(R.id.det_pago);
                    TextView txt_tiempo = findViewById(R.id.det_tiempo_tras);
                    txt_pago.append("Error al determinar");
                    txt_tiempo.append("Error al determinar");
                    txt_tarifando.append("Error al determinar tarifa");
                    btn_de_despacho.setVisibility(View.GONE);
                    Log.d(TAG, "ERROR VOLLEY " + error.toString());
                    NetworkResponse response = error.networkResponse;
                    if (error instanceof NetworkError) {

                    } else if (error instanceof ServerError) {
                        String res = null;
                        try {
                            res = new String(response.data, HttpHeaderParser.parseCharset(response.headers, "utf-8"));
                            Log.d(TAG, "onErrorResponse: " + res);
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }
                        Log.e(TAG, "Server Error: " + error.networkResponse.statusCode);
                    } else if (error instanceof AuthFailureError) {
                        Log.e(TAG, "AuthFailureError: " + error.getMessage());
                    } else if (error instanceof ParseError) {
                        Log.e(TAG, "ParseError: " + error.getMessage());
                    } else if (error instanceof TimeoutError) {
                        Log.e(TAG, "TimeoutError " + error.getMessage());
                    }
                }
            }) {
                @Override
                public Map<String, String> getHeaders() {
                    Map<String, String> cabeceras = new Hashtable<>();
                    cabeceras.put("Content-Type", "application/json; charset=utf-8");
                    return cabeceras;
                }
            };
            RequestQueue requestQueue = volleySingleton.getmRequestQueue();
            requestQueue.add(peticion);
        } else {
            Toast.makeText(this, "Ip del servidor no definida en Configuración", Toast.LENGTH_SHORT).show();
        }
    }
}