package cl.pall.jesuspons.jesuspons.Modelos;

/**
 * Creado por Nicolás en 25/04/2018.
 */
public class Token {
    private String id_usuario;
    private String usuario;
    private String token;

    public Token() {
    }

    public Token(String id_usuario, String usuario, String token) {
        this.id_usuario = id_usuario;
        this.usuario = usuario;
        this.token = token;
    }

    public String getId_usuario() {
        return id_usuario;
    }

    public void setId_usuario(String id_usuario) {
        this.id_usuario = id_usuario;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
