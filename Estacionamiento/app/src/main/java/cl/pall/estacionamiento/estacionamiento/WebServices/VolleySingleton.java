package cl.pall.estacionamiento.estacionamiento.WebServices;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.LruCache;
import com.android.volley.Cache;
import com.android.volley.Network;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.BasicNetwork;
import com.android.volley.toolbox.DiskBasedCache;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.ImageLoader;

/**
 * Creado por Nicolás Galdames en 27/09/2018.
 */
public class VolleySingleton {
    private static VolleySingleton volleyS = null;
    private static Context context;

    private RequestQueue mRequestQueue;
    private ImageLoader imageLoader;

    public VolleySingleton(Context context) {
        this.context = context;
        this.mRequestQueue = getmRequestQueue();
        //mRequestQueue = Volley.newRequestQueue(context);

        imageLoader = new ImageLoader(mRequestQueue,
            new ImageLoader.ImageCache() {
                private final LruCache<String, Bitmap>
                        cache = new LruCache<>(10);

                @Override
                public Bitmap getBitmap(String url) {
                    return cache.get(url);
                }

                @Override
                public void putBitmap(String url, Bitmap bitmap) {
                    cache.put(url, bitmap);
                }
            });
    }

    public static VolleySingleton getInstance(Context context) {
        if (volleyS == null) {
            volleyS = new VolleySingleton(context);
        }
        return volleyS;
    }

    public RequestQueue getmRequestQueue() {
        if (mRequestQueue == null) {
            Cache cache = new DiskBasedCache(context.getCacheDir(), 10 * 1024 * 1024);
            Network network = new BasicNetwork(new HurlStack());
            mRequestQueue = new RequestQueue(cache, network);
            mRequestQueue.start();
        }
        return mRequestQueue;
    }

    public <T> void addToRequestQueue(Request<T> req) {
        getmRequestQueue().add(req);
    }

    public ImageLoader getImageLoader() {
        return imageLoader;
    }
}