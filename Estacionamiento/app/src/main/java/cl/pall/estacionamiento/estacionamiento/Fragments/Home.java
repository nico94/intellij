package cl.pall.estacionamiento.estacionamiento.Fragments;

import android.app.DatePickerDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.*;
import android.view.inputmethod.InputMethodManager;
import android.widget.*;
import cl.pall.estacionamiento.estacionamiento.Adaptadores.Adp_registros;
import cl.pall.estacionamiento.estacionamiento.Modelos.Transaccion;
import cl.pall.estacionamiento.estacionamiento.R;
import cl.pall.estacionamiento.estacionamiento.Utils.Constant;
import com.google.firebase.database.*;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Home extends Fragment implements SearchView.OnQueryTextListener, MenuItem.OnActionExpandListener {
    private static final String TAG = Home.class.getSimpleName();
    private View vistaRoot;
    private Adp_registros adp_registros;
    private List<Transaccion> listaTransacciones = new ArrayList<Transaccion>();
    private GridLayoutManager gridManager;
    private RecyclerView contenedor_transacciones;
    private FirebaseDatabase database;
    private RadioGroup radioGroup;
    private Context context;
    private ProgressBar carga;
    private EditText edt_fecha;
    private int tipoRad;

    public Home() {
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = getActivity();
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        vistaRoot = inflater.inflate(R.layout.fragment_home, container, false);
        gridManager = new GridLayoutManager(getActivity(), 1);
        carga = vistaRoot.findViewById(R.id.cargando);
        contenedor_transacciones = vistaRoot.findViewById(R.id.contenedor_transacciones);
        contenedor_transacciones.setHasFixedSize(true);
        contenedor_transacciones.setLayoutManager(gridManager);
        edt_fecha = vistaRoot.findViewById(R.id.edt_fecha_vehiculo);
        adp_registros = new Adp_registros(getActivity(), listaTransacciones);
        adp_registros.notifyDataSetChanged();
        contenedor_transacciones.setAdapter(adp_registros);
        contenedor_transacciones.setVisibility(View.GONE);
        database = FirebaseDatabase.getInstance();
        radioGroup = vistaRoot.findViewById(R.id.rdg_estados);
        Date dat = new Date();
        String fecha = (String) android.text.format.DateFormat.format("dd-MM-yyyy", dat.getTime());
        edt_fecha.setText(fecha);
        eventos();
        cargarRegistrosFirebase(0, fecha);
        return vistaRoot;
    }

    private void eventos() {
        edt_fecha.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DatePickerFragment newFragment = DatePickerFragment.newInstance(new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker datePicker, int ano, int mes, int dia) {
                        carga.setVisibility(View.VISIBLE);
                        final String fechaSeleccion = dia + "-" + (mes + 1) + "-" + ano;
                        cargarRegistrosFirebase(tipoRad,fechaSeleccion);
                        Log.d(TAG, "selec : " + fechaSeleccion.replace("-",""));
                        edt_fecha.setText(fechaSeleccion);
                    }
                });
                newFragment.show(getActivity().getSupportFragmentManager(), "datePicker");
            }
        });

        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.rad_pendientes:
                        tipoRad = 0;
                        carga.setVisibility(View.VISIBLE);
                        contenedor_transacciones.setVisibility(View.GONE);
                        cargarRegistrosFirebase(tipoRad, edt_fecha.getText().toString());
                        break;
                    case R.id.rad_despachados:
                        tipoRad = 1;
                        carga.setVisibility(View.VISIBLE);
                        contenedor_transacciones.setVisibility(View.GONE);
                        cargarRegistrosFirebase(tipoRad, edt_fecha.getText().toString());
                        break;
                    default:
                        break;
                }
            }
        });
    }

    private void cargarRegistrosFirebase(final int tipo, String fecha) {
        final DatabaseReference registrosRef = database.getReference().child(Constant.NODO_FIREBASE).child(fecha.replace("-",""));
        registrosRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                listaTransacciones = new ArrayList<>();
                for (DataSnapshot regSnapshot : dataSnapshot.getChildren()) {
                    Transaccion transaccion = regSnapshot.getValue(Transaccion.class);
                    assert transaccion != null;
                    if (transaccion.getEstado() == tipo && transaccion.getId() != null) {
                        listaTransacciones.add(transaccion);
                    }
                }
                adp_registros = new Adp_registros(getContext(), listaTransacciones);
                contenedor_transacciones.setAdapter(adp_registros);
                contenedor_transacciones.setVisibility(View.VISIBLE);
                carga.setVisibility(View.GONE);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private void cargarRegistrosFirebase1(final int tipo) {
        DatabaseReference reference = FirebaseDatabase.getInstance().getReference();
        Query query = reference.child(Constant.NODO_FIREBASE).orderByChild("estado").equalTo(tipo);
        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                listaTransacciones = new ArrayList<>();
                if (dataSnapshot.exists()) {
                    for (DataSnapshot regSnapshot : dataSnapshot.getChildren()) {
                        Transaccion transaccion = regSnapshot.getValue(Transaccion.class);
                        listaTransacciones.add(transaccion);
                    }
                    adp_registros = new Adp_registros(getContext(), listaTransacciones);
                    contenedor_transacciones.setAdapter(adp_registros);
                    contenedor_transacciones.setVisibility(View.VISIBLE);
                    carga.setVisibility(View.GONE);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.d(TAG, "CANCELADA: " + databaseError.toString());
                Toast.makeText(context, "Error en base de datos", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        inflater.inflate(R.menu.search_menu, menu);
        MenuItem searchItem = menu.findItem(R.id.action_search);
        SearchView searchView = (SearchView) searchItem.getActionView();
        searchView.setOnQueryTextListener(this);
        searchView.setQueryHint("Buscar patentes...");
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onMenuItemActionExpand(MenuItem item) {
        return true;
    }

    @Override
    public boolean onMenuItemActionCollapse(MenuItem item) {
        return true;
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        adp_registros.getFilter().filter(query);
        View view = getActivity().getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            assert imm != null;
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
        return true;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        adp_registros.getFilter().filter(newText);
        return false;
    }
}