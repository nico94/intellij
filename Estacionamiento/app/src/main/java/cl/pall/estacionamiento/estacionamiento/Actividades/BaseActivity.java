package cl.pall.estacionamiento.estacionamiento.Actividades;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.RemoteException;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.inputmethod.InputMethodManager;

import android.widget.Toast;
import com.smartdevice.aidl.IZKCService;

import cl.pall.estacionamiento.estacionamiento.common.MessageCenter;
import cl.pall.estacionamiento.estacionamiento.common.MessageType;

public class BaseActivity extends AppCompatActivity {
    final static String TAG = "BaseActivity";
    public static String MODULE_FLAG = "module_flag";
    public static int module_flag = 0;
    public static int DEVICE_MODEL = 0;
    private Handler mhanlder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG, "BASE ACTIVITY: ON CREATE");
        MessageCenter.getInstance().addHandler(getHandler());
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        assert imm != null;
        imm.toggleSoftInput(0, InputMethodManager.HIDE_NOT_ALWAYS);
        module_flag = getIntent().getIntExtra(MODULE_FLAG, 4);
//        module_flag = 4;
        bindService();
    }

    protected void handleStateMessage(Message message) {
    }

    @SuppressLint("HandlerLeak")
    protected Handler getHandler() {
        if (mhanlder == null) {
            mhanlder = new Handler() {
                public void handleMessage(Message msg) {
                    handleStateMessage(msg);
                }
            };
        }
        return mhanlder;
    }

    protected void sendMessage(Message message) {
        getHandler().sendMessage(message);
    }

    protected void sendMessage(int what, Object obj) {
        Message message = new Message();
        message.what = what;
        message.obj = obj;
        Log.d(TAG, "sendMessage: "+message.toString());
        getHandler().sendMessage(message);
    }

    protected void sendEmptyMessage(int what) {
        getHandler().sendEmptyMessage(what);
    }

    public static IZKCService mIzkcService;
    private ServiceConnection mServiceConn = new ServiceConnection() {
        @Override
        public void onServiceDisconnected(ComponentName name) {
            Log.d("client", "SERVICIO DESCONECTADO");
//            Toast.makeText(BaseActivity.this, "Servicio desconectado", Toast.LENGTH_SHORT).show();
            mIzkcService = null;
            sendEmptyMessage(MessageType.BaiscMessage.SEVICE_BIND_FAIL);
        }

        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            Log.d("client", "SERVICIO CONECTADO");
//            Toast.makeText(BaseActivity.this, "Servicio conectado", Toast.LENGTH_SHORT).show();
            mIzkcService = IZKCService.Stub.asInterface(service);
            if (mIzkcService != null) {
                try {
                    DEVICE_MODEL = mIzkcService.getDeviceModel();
                    mIzkcService.setModuleFlag(module_flag);
                } catch (RemoteException e) {
                    e.printStackTrace();
                }
                sendEmptyMessage(MessageType.BaiscMessage.SEVICE_BIND_SUCCESS);
            }
        }
    };

    public void bindService() {
        Intent intent = new Intent("com.zkc.aidl.all");
        intent.setPackage("com.smartdevice.aidl");
        bindService(intent, mServiceConn, Context.BIND_AUTO_CREATE);
    }

    public void unbindService() {
        Log.d(TAG, "DESCONECTAR SERVICIO");
        unbindService(mServiceConn);
    }

    @Override
    protected void onResume() {
        super.onResume();
//        MessageCenter.getInstance().addHandler(getHandler());
//        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
//        assert imm != null;
//        imm.toggleSoftInput(0, InputMethodManager.HIDE_NOT_ALWAYS);
//        module_flag = getIntent().getIntExtra(MODULE_FLAG, 4);
//        bindService();
    }

    @Override
    protected void onDestroy() {
        unbindService();
        super.onDestroy();
    }

    protected Context getDialogContext() {
        Activity activity = this;
        while (activity.getParent() != null) {
            activity = activity.getParent();
        }
        Log.d("Dialog", "context:" + activity);
        return activity;
    }
}