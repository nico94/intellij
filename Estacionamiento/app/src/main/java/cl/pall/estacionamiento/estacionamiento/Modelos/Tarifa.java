package cl.pall.estacionamiento.estacionamiento.Modelos;

public class Tarifa {
    private int id;
    private String descripcion;
    private String fecha;
    private int valor;

    public Tarifa() {
    }

    public Tarifa(int id, String descripcion, String fecha, int valor) {
        this.id = id;
        this.descripcion = descripcion;
        this.fecha = fecha;
        this.valor = valor;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public int getValor() {
        return valor;
    }

    public void setValor(int valor) {
        this.valor = valor;
    }
}