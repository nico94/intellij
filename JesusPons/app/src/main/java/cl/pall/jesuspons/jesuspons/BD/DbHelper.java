package cl.pall.jesuspons.jesuspons.BD;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import cl.pall.jesuspons.jesuspons.Modelos.Tarea;
import cl.pall.jesuspons.jesuspons.Modelos.Usuario;

/**
 * Creado por Nicolás en 13/04/2018.
 */
public class DbHelper extends SQLiteOpenHelper {
    private static final String TAG = DbHelper.class.getSimpleName();
    private static final String DB_NAME = "JesusPons.db";
    private static final int DB_VERSION = 4;

    public DbHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(Sql.CREAR_TABLA_USUARIO);
        db.execSQL(Sql.CREAR_TABLA_TAREAS);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS Usuario");
        db.execSQL("DROP TABLE IF EXISTS Tareas");
        onCreate(db);
    }

    ///////////////////////////
    // METODOS BASE DE DATOS //
    ///////////////////////////

    ///////////////////
    // TABLA USUARIO //
    ///////////////////
    public void agregaUsuario(Usuario usuario) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(Sql.COL_CODIGO, usuario.getCodigo());
        values.put(Sql.COL_COD_CARGO, usuario.getCod_cargo());
        values.put(Sql.COL_USUARIO, usuario.getUsuario());
        values.put(Sql.COL_PASSWORD, usuario.getPassword());
        values.put(Sql.COL_RUT, usuario.getRut());
        values.put(Sql.COL_NOMBRES, usuario.getNombres());
        values.put(Sql.COL_AP_PATERNO, usuario.getAp_paterno());
        values.put(Sql.COL_AP_MATERNO, usuario.getAp_materno());
        values.put(Sql.COL_FECHA_NACIMIENTO, usuario.getFecha_nacimiento());
        values.put(Sql.COL_EMAIL, usuario.getEmail());
        values.put(Sql.COL_TELEFONO, usuario.getTelefono());
        values.put(Sql.COL_DIRECCION, usuario.getDireccion());
        values.put(Sql.COL_COD_COMUNA, usuario.getCod_comuna());
        db.insert(Sql.TABLA_USUARIO, null, values);
        db.close();
        Log.d(TAG, "USUARIO AGREGADO:" +
                " COD: " + usuario.getCodigo() +
                " CARGO: " + usuario.getCod_cargo() +
                " USU: " + usuario.getUsuario() +
                " PASS: " + usuario.getPassword() +
                " RUT: " + usuario.getRut() +
                " NOMBRES: " + usuario.getNombres() +
                " PATERNO: " + usuario.getAp_paterno() +
                " MATERNO: " + usuario.getAp_materno() +
                " FECHA_NACIMIENTO: " + usuario.getFecha_nacimiento() +
                " EMAIL: " + usuario.getEmail() +
                " TELEFONO: " + usuario.getTelefono() +
                " DIRECCION: " + usuario.getDireccion() +
                " COD_COMUNA: " + usuario.getCod_comuna());
    }

    public Usuario obtenerUsuario() {
        Usuario usuario = new Usuario();
        String selectQuery = "SELECT * FROM " + Sql.TABLA_USUARIO;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor != null && cursor.moveToFirst()) {
            usuario = new Usuario(
                    cursor.getInt(0),
                    cursor.getInt(1),
                    cursor.getInt(2),
                    cursor.getString(3),
                    cursor.getString(4),
                    cursor.getString(5),
                    cursor.getString(6),
                    cursor.getString(7),
                    cursor.getString(8),
                    cursor.getString(9),
                    cursor.getString(10),
                    cursor.getString(11),
                    cursor.getString(12),
                    cursor.getInt(13)
            );
            cursor.close();
        }
//        Log.d(TAG, "USUARIO LEIDO:" +
//                " ID: " + usuario.getId() +
//                " COD: " + usuario.getCodigo() +
//                " CARGO: " + usuario.getCod_cargo() +
//                " USU: " + usuario.getUsuario() +
//                " PASS: " + usuario.getPassword() +
//                " RUT: " + usuario.getRut() +
//                " NOMBRES: " + usuario.getNombres() +
//                " PATERNO: " + usuario.getAp_paterno() +
//                " MATERNO: " + usuario.getAp_materno() +
//                " NACIMIENTO: " + usuario.getFecha_nacimiento() +
//                " EMAIL: " + usuario.getEmail() +
//                " TELEFONO: " + usuario.getTelefono() +
//                " DIRECCION: " + usuario.getDireccion() +
//                " COD_COMUNA: " + usuario.getCod_comuna());
        db.close();
        return usuario;
    }

    public void modificaUsuario(Usuario usuario) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(Sql.COL_CODIGO, usuario.getCodigo());
        values.put(Sql.COL_COD_CARGO, usuario.getCod_cargo());
        values.put(Sql.COL_USUARIO, usuario.getUsuario());
        values.put(Sql.COL_PASSWORD, usuario.getPassword());
        values.put(Sql.COL_RUT, usuario.getRut());
        values.put(Sql.COL_NOMBRES, usuario.getNombres());
        values.put(Sql.COL_AP_PATERNO, usuario.getAp_paterno());
        values.put(Sql.COL_AP_MATERNO, usuario.getAp_materno());
        values.put(Sql.COL_FECHA_NACIMIENTO, usuario.getFecha_nacimiento());
        values.put(Sql.COL_EMAIL, usuario.getEmail());
        values.put(Sql.COL_TELEFONO, usuario.getTelefono());
        values.put(Sql.COL_DIRECCION, usuario.getDireccion());
        values.put(Sql.COL_COD_COMUNA, usuario.getCod_comuna());
        db.update(Sql.TABLA_USUARIO, values, Sql.COL_RUT + "='" + usuario.getRut() + "'", null);
        db.close();
//        Log.d(TAG, "MODIFICA USUARIO:" +
//                " ID: " + usuario.getId() +
//                " COD: " + usuario.getCodigo() +
//                " CARGO: " + usuario.getCod_cargo() +
//                " USU: " + usuario.getUsuario() +
//                " PASS: " + usuario.getPassword() +
//                " RUT: " + usuario.getRut() +
//                " NOMBRES: " + usuario.getNombres() +
//                " PATERNO: " + usuario.getAp_paterno() +
//                " MATERNO: " + usuario.getAp_materno() +
//                " NACIMIENTO: " + usuario.getFecha_nacimiento() +
//                " EMAIL: " + usuario.getEmail() +
//                " TELEFONO: " + usuario.getTelefono() +
//                " DIRECCION: " + usuario.getDireccion() +
//                " COD_COMUNA: " + usuario.getCod_comuna());
    }

    public void eliminarUsuario() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(Sql.TABLA_USUARIO, null, null);
        db.close();
        Log.d(TAG, "ELIMINA TABLA: " + Sql.TABLA_USUARIO);
    }

    //////////////////
    // TABLA TAREAS //
    //////////////////
    public void agregarTarea(Tarea tareas) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(Sql.COL_ID_ACCION, tareas.getId_accion());
        values.put(Sql.COL_ID_CIERRE, tareas.getId_cierre_negocio());
        values.put(Sql.COL_COD_FICHA_PERSONAL, tareas.getCod_ficha_personal());
        values.put(Sql.COL_RESPONSABLE, tareas.getResponsable().trim());
        values.put(Sql.COL_ESTADO, tareas.getEstado());
        values.put(Sql.COL_LECTURA, tareas.getLectura());
        values.put(Sql.COL_DETALLE, tareas.getDetalle().trim());
        values.put(Sql.COL_MARCA, tareas.getMarca().trim());
        values.put(Sql.COL_MODELO, tareas.getModelo().trim());
        values.put(Sql.COL_VIN, tareas.getVin().trim());
        values.put(Sql.COL_FECHA_CREA, tareas.getFecha_crea());
        values.put(Sql.COL_HORA_CREA, tareas.getHora_crea());
        values.put(Sql.COL_FECHA_EXP, tareas.getFecha_exp());
        values.put(Sql.COL_HORA_EXP, tareas.getHora_exp());
        values.put(Sql.COL_FECHA_INI, tareas.getFecha_ini());
        values.put(Sql.COL_HORA_INI, tareas.getHora_ini());
        values.put(Sql.COL_FECHA_FIN, tareas.getFecha_fin());
        values.put(Sql.COL_HORA_FIN, tareas.getHora_fin());
        db.insert(Sql.TABLA_TAREAS, null, values);
        db.close();
        Log.d(TAG, "AGREGA TAREA: ID: " + tareas.getId()
                + " COD ACCION: " + tareas.getId_accion()
                + " CIERRE: " + tareas.getId_cierre_negocio()
                + " COD FICHA: " + tareas.getCod_ficha_personal()
                + " RESPONSABLE: " + tareas.getResponsable()
                + " ESTADO: " + tareas.getEstado()
                + " LECTURA: " + tareas.getLectura()
                + " DETALLE: " + tareas.getDetalle()
                + " MARCA: " + tareas.getMarca()
                + " MODELO: " + tareas.getModelo()
                + " VIN: " + tareas.getVin()
                + " FECHA CREA: " + tareas.getFecha_crea()
                + " HORA CREA: " + tareas.getHora_crea()
                + " FECHA EXP: " + tareas.getFecha_exp()
                + " HORA EXP: " + tareas.getHora_exp()
                + " FECHA INI: " + tareas.getFecha_ini()
                + " HORA INI: " + tareas.getHora_ini()
                + " FECHA FIN: " + tareas.getFecha_fin()
                + " HORA FIN: " + tareas.getHora_fin()
        );
    }

    public boolean modificarTarea(Tarea tareas) {
        boolean respuesta = false;
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(Sql.COL_ID_ACCION, tareas.getId_accion());
        values.put(Sql.COL_ID_CIERRE, tareas.getId_cierre_negocio());
        values.put(Sql.COL_COD_FICHA_PERSONAL, tareas.getCod_ficha_personal());
        values.put(Sql.COL_RESPONSABLE, tareas.getResponsable().trim());
        values.put(Sql.COL_ESTADO, tareas.getEstado());
        values.put(Sql.COL_LECTURA, tareas.getLectura());
        values.put(Sql.COL_DETALLE, tareas.getDetalle().trim());
        values.put(Sql.COL_MARCA, tareas.getMarca().trim());
        values.put(Sql.COL_MODELO, tareas.getModelo().trim());
        values.put(Sql.COL_VIN, tareas.getVin().trim());
        values.put(Sql.COL_FECHA_CREA, tareas.getFecha_crea());
        values.put(Sql.COL_HORA_CREA, tareas.getHora_crea());
        values.put(Sql.COL_FECHA_EXP, tareas.getFecha_exp());
        values.put(Sql.COL_HORA_EXP, tareas.getHora_exp());
        values.put(Sql.COL_FECHA_INI, tareas.getFecha_ini());
        values.put(Sql.COL_HORA_INI, tareas.getHora_ini());
        values.put(Sql.COL_FECHA_FIN, tareas.getFecha_fin());
        values.put(Sql.COL_HORA_FIN, tareas.getHora_fin());
        int rows = db.update(Sql.TABLA_TAREAS, values,
                Sql.COL_ID_ACCION + " = '" + tareas.getId_accion() + "' AND " + Sql.COL_ID_CIERRE + " = '" + tareas.getId_cierre_negocio() + "' " +
                        "AND " + Sql.COL_COD_FICHA_PERSONAL + " = '" + tareas.getCod_ficha_personal() + "'"
                , null);
        db.close();
//        Log.d(TAG, "MODIFICA TAREA: " + tareas.getId() + " ESTADO: " + tareas.getEstado());
        if (rows > 0) {
            respuesta = true;
        }
        return respuesta;
    }

    public boolean verificaExisteTarea(Tarea tarea) {
        String selectQuery = "SELECT * FROM " + Sql.TABLA_TAREAS + " WHERE " + Sql.COL_ID_ACCION + " = '" + tarea.getId_accion() + "' " +
                "AND " + Sql.COL_ID_CIERRE + " = '" + tarea.getId_cierre_negocio() + "' AND " + Sql.COL_COD_FICHA_PERSONAL + " = '" + tarea.getCod_ficha_personal() + "'";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        int contador = cursor.getCount();
        cursor.close();
        db.close();
        return contador > 0;
    }

    public List<Tarea> obtenerTareas() {
        List<Tarea> tareasList = new ArrayList<Tarea>();
        String selectQuery = "SELECT * FROM " + Sql.TABLA_TAREAS + " ORDER BY id DESC";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                Tarea tareas = new Tarea();
                tareas.setId(cursor.getInt(0));
                tareas.setId_accion(cursor.getInt(1));
                tareas.setId_cierre_negocio(cursor.getInt(2));
                tareas.setCod_ficha_personal(cursor.getInt(3));
                tareas.setResponsable(cursor.getString(4));
                tareas.setEstado(cursor.getInt(5));
                tareas.setLectura(cursor.getInt(6));
                tareas.setDetalle(cursor.getString(7));
                tareas.setMarca(cursor.getString(8));
                tareas.setModelo(cursor.getString(9));
                tareas.setVin(cursor.getString(10));
                tareas.setFecha_crea(cursor.getString(11));
                tareas.setHora_crea(cursor.getString(12));
                tareas.setFecha_exp(cursor.getString(13));
                tareas.setHora_exp(cursor.getString(14));
                tareas.setFecha_ini(cursor.getString(15));
                tareas.setHora_ini(cursor.getString(16));
                tareas.setFecha_fin(cursor.getString(17));
                tareas.setHora_fin(cursor.getString(18));
                tareasList.add(tareas);
            } while (cursor.moveToNext());
        }
        db.close();
        return tareasList;
    }

    public List<Tarea> obtenerTareasEstado(int estado, int cod_ficha_personal) {
        List<Tarea> tareasList = new ArrayList<Tarea>();
        String selectQuery = "SELECT * FROM " + Sql.TABLA_TAREAS + " WHERE " + Sql.COL_COD_FICHA_PERSONAL + "='" + cod_ficha_personal + "' AND " + Sql.COL_ESTADO + "='" + estado + "' ORDER By id DESC";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                Tarea tareas = new Tarea();
                tareas.setId(cursor.getInt(0));
                tareas.setId_accion(cursor.getInt(1));
                tareas.setId_cierre_negocio(cursor.getInt(2));
                tareas.setCod_ficha_personal(cursor.getInt(3));
                tareas.setResponsable(cursor.getString(4));
                tareas.setEstado(cursor.getInt(5));
                tareas.setLectura(cursor.getInt(6));
                tareas.setDetalle(cursor.getString(7));
                tareas.setMarca(cursor.getString(8));
                tareas.setModelo(cursor.getString(9));
                tareas.setVin(cursor.getString(10));
                tareas.setFecha_crea(cursor.getString(11));
                tareas.setHora_crea(cursor.getString(12));
                tareas.setFecha_exp(cursor.getString(13));
                tareas.setHora_exp(cursor.getString(14));
                tareas.setFecha_ini(cursor.getString(15));
                tareas.setHora_ini(cursor.getString(16));
                tareas.setFecha_fin(cursor.getString(17));
                tareas.setHora_fin(cursor.getString(18));
                tareasList.add(tareas);
            } while (cursor.moveToNext());
        }
        db.close();
        return tareasList;
    }

    public Tarea obtenerTarea(int id) {
        Tarea tareas = new Tarea();
        String query = "SELECT * FROM " + Sql.TABLA_TAREAS + " WHERE " + Sql.ID + "='" + id + "'";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(query, null);
        if (cursor != null && cursor.moveToFirst()) {
            tareas = new Tarea(
                    cursor.getInt(0),
                    cursor.getInt(1),
                    cursor.getInt(2),
                    cursor.getInt(3),
                    cursor.getString(4),
                    cursor.getInt(5),
                    cursor.getInt(6),
                    cursor.getString(7),
                    cursor.getString(8),
                    cursor.getString(9),
                    cursor.getString(10),
                    cursor.getString(11),
                    cursor.getString(12),
                    cursor.getString(13),
                    cursor.getString(14),
                    cursor.getString(15),
                    cursor.getString(16),
                    cursor.getString(17),
                    cursor.getString(18)
            );
            cursor.close();
        }
        Log.d(TAG, "TAREA LEIDA: " + tareas.getDetalle());
        db.close();
        return tareas;
    }

    public int totalTareasPorEstado(int estado) {
        int total = 0;
        String selectQuery = "SELECT * FROM " + Sql.TABLA_TAREAS + " WHERE " + Sql.COL_ESTADO + "='" + estado + "'";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                total += 1;
            } while (cursor.moveToNext());
        }
        cursor.close();
        db.close();
        return total;
    }

    public void eliminarTareas() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(Sql.TABLA_TAREAS, null, null);
        db.close();
        Log.d(TAG, "ELIMINA TABLA: " + Sql.TABLA_TAREAS);
    }
}