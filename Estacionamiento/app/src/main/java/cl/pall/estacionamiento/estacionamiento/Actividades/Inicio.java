package cl.pall.estacionamiento.estacionamiento.Actividades;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaPlayer;
import android.os.*;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.*;
import cl.pall.estacionamiento.estacionamiento.Fragments.Configuracion;
import cl.pall.estacionamiento.estacionamiento.Fragments.Despacho;
import cl.pall.estacionamiento.estacionamiento.Fragments.Home;
import cl.pall.estacionamiento.estacionamiento.Fragments.Ingreso;
import cl.pall.estacionamiento.estacionamiento.Modelos.Transaccion;
import cl.pall.estacionamiento.estacionamiento.R;
import cl.pall.estacionamiento.estacionamiento.Scanner.ClientConfig;
import cl.pall.estacionamiento.estacionamiento.Utils.Constant;
import cl.pall.estacionamiento.estacionamiento.Utils.Funciones;
import cl.pall.estacionamiento.estacionamiento.WebServices.VolleySingleton;
import cl.pall.estacionamiento.estacionamiento.common.MessageType;
import com.android.volley.*;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.firebase.database.*;
import com.smartdevice.aidl.ICallBack;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.UnsupportedEncodingException;
import java.util.*;

public class Inicio extends BaseActivity implements NavigationView.OnNavigationItemSelectedListener {
    private final static String TAG = Inicio.class.getSimpleName();
    private int REQUEST_CODE = 3;
    private Fragment fragment = null;
    private View vista;
    private String escaneado = null;
    private String fechaDespacho = null;
    private String horaDespacho = null;
    private int horas = 0;
    private int minutos = 0;
    private double tarifa = 6.666666666666667; //400 la hora
    private String imagenBase64 = "";
    private NavigationView navigationView;
    private static long back_pressed;

    // Web Services
    private VolleySingleton volleySingleton;
    private RequestQueue requestQueue;

    // Para scanner
    public String text = "";
    private String firstCodeStr = "";
    private MediaPlayer player;
    private Vibrator vibrator;
    private Bitmap bitmap_foto = null;

    private boolean tar_estado = false;
    private String tar_mensaje = null;
    private String tar_fecha_entrada = null;
    private String tar_hora_entrada = null;
    private String tar_fecha_salida = null;
    private String tar_hora_salida = null;
    private String tar_id_transaccion = null;
    private String tar_patente = null;
    private int tar_total_cobro = 0;
    private String tar_tiempo_transcurrido = "";
    private String tar_imagen = "";

    private boolean modo_telefono, imprime_ingreso, imprime_despacho, menu_ingreso, menu_despacho, imprime_qr;

    //FIREBASE
    private FirebaseDatabase database = FirebaseDatabase.getInstance();

    private ICallBack.Stub mCallback = new ICallBack.Stub() {
        @Override
        public void onReturnValue(byte[] buffer, int size) throws RemoteException {
            String codeStr = new String(buffer, 0, size);
            if (ClientConfig.getBoolean(ClientConfig.SCAN_REPEAT)) {
                if (firstCodeStr.equals(codeStr)) {
                    vibrator.vibrate(100);
                }
            }
            if (ClientConfig.getBoolean(ClientConfig.APPEND_RINGTONE)) {
                player.start();
            }
            if (ClientConfig.getBoolean(ClientConfig.APPEND_VIBRATE)) {
                vibrator.vibrate(100);
            }
            firstCodeStr = codeStr;
            sendMessage(MessageType.BaiscMessage.SCAN_RESULT_GET_SUCCESS, codeStr);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inicio);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        //Inicializa componentes
        vista = findViewById(android.R.id.content);
        player = MediaPlayer.create(getApplicationContext(), R.raw.scan);
        vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
        //Inicializa BD
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        View header = navigationView.getHeaderView(0);
        if (database == null) {
            Log.d(TAG, "BASE DE DATOS PERSISTENTE");
            database = FirebaseDatabase.getInstance();
            database.setPersistenceEnabled(true);
        }
        leerConfiguracion();
        // Lector de barras
        inciarLectorBarras();
        //Inicializar Volley
        volleySingleton = VolleySingleton.getInstance(getApplicationContext().getApplicationContext());
        requestQueue = volleySingleton.getmRequestQueue();
        fragmentDefault();
    }

    private void leerConfiguracion() {
        SharedPreferences pref = getApplicationContext().getSharedPreferences(getString(R.string.PREF), Context.MODE_PRIVATE);
        modo_telefono = Boolean.parseBoolean(pref.getString("modo_telefono", ""));
        imprime_ingreso = Boolean.parseBoolean(pref.getString("imp_ingreso", ""));
        imprime_despacho = Boolean.parseBoolean(pref.getString("imp_despacho", ""));
        menu_ingreso = Boolean.parseBoolean(pref.getString("menu_ingreso", ""));
        menu_despacho = Boolean.parseBoolean(pref.getString("menu_despacho", ""));
        imprime_qr = Boolean.parseBoolean(pref.getString("imp_qr", ""));
        if (navigationView != null) {
            Menu menuNav = navigationView.getMenu();
            menuNav.findItem(R.id.nav_ingreso).setVisible(menu_ingreso);
            menuNav.findItem(R.id.nav_despacho).setVisible(menu_despacho);
        }
    }

    private void fragmentDefault() {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        Home home = new Home();
        fragment = home;
        Objects.requireNonNull(getSupportActionBar()).setTitle("Inicio");
        navigationView.getMenu().getItem(0).setChecked(true);
        transaction.replace(R.id.contenedor_fragments, home);
        transaction.commit();
    }

    ///////////////////////
    // METODOS GENERALES //
    ///////////////////////
    private void enviarFragment(String escaneo) {
        if (!TextUtils.isEmpty(escaneo)) {
            if (escaneo.trim().length() > 13 && !escaneo.contains(" ")) {
                escaneo = escaneo.substring(0, 13);
            }
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            Despacho frg_despacho = new Despacho();
            fragment = frg_despacho;
            if (escaneo.trim().length() == 13) {
                Bundle datos = new Bundle();
                datos.putString("escaneado", escaneo);
                frg_despacho.setArguments(datos);
                transaction.replace(R.id.contenedor_fragments, frg_despacho);
                transaction.commitAllowingStateLoss();
            }
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            if (back_pressed + 2000 > System.currentTimeMillis()) {
                super.onBackPressed();
            } else {
                Toast.makeText(getBaseContext(), "Vuelva a presionar para salir", Toast.LENGTH_SHORT).show();
                back_pressed = System.currentTimeMillis();
                fragmentDefault();
            }
        }
    }

    //////////////////////////////
    // METODOS FRAGMENT INGRESO //
    //////////////////////////////
    public void ingresarVehiculo() {
        cerrarTeclado();
        leerConfiguracion();
        String patente;
        EditText edt_patente = findViewById(R.id.edt_patente);
        if (!TextUtils.isEmpty(edt_patente.getText().toString())) {
            patente = edt_patente.getText().toString();
            if (patente.length() == 6) {
                String id = null;
                Date dat = new Date();
                String fecha = (String) android.text.format.DateFormat.format("dd-MM-yyyy", dat.getTime());
                String hora = (String) android.text.format.DateFormat.format("HH:mm:ss", dat.getTime());
                Transaccion transaccion = new Transaccion();
                transaccion.setEstado(0);
                transaccion.setPatente(patente);
                transaccion.setFechaIngreso(fecha);
                transaccion.setHoraIngreso(hora);
                transaccion.setFechaSalida("00-00-0000");
                transaccion.setHoraSalida("00:00:00");
                transaccion.setPago(0);
                if (imagenBase64.length() > 0) {
                    transaccion.setImagen(imagenBase64);
                }
                id = ingresarTransaccionFirebase(transaccion);
                if (id != null) {
                    transaccion.setId(id);
                    mostrarSnack(0, "Ingresado correctamente");
                    edt_patente.requestFocus();
                    if (imprime_ingreso) {
                        imprimirTicket(transaccion);
                    }
                }
                Button btn = findViewById(R.id.btn_foto);
                btn.setText("Sin Foto");
                edt_patente.setText("");
                bitmap_foto = null;
                imagenBase64 = "";
            } else {
                mostrarSnack(1, "El largo de patente no corresponde");
            }
        }
    }

    private String ingresarTransaccionFirebase(Transaccion transaccion) {
        String id;
        String fecha = transaccion.getFechaIngreso().replace("-", "");
        id = fecha + Funciones.random();
        //DatabaseReference referenciaDB = database.getReference().child(Constant.NODO_FIREBASE).child(id);
        DatabaseReference referenciaDB = database.getReference().child(Constant.NODO_FIREBASE).child(fecha).child(id);
        transaccion.setId(id);
        referenciaDB.setValue(transaccion);
        return id;
    }

    public void imprimirTicket(Transaccion transaccion) {
        if (DEVICE_MODEL > 0) {
            BaseActivity.module_flag = 0;
            Bitmap bitmap = null;
            String codigo = transaccion.getId();
            //Log.d(TAG, "imprimirTicket: " + codigo.length() + "|" + codigo);
            String limpio = transaccion.getPatente().replace("\n", "").replace("\r", "");
            if (imprime_qr) {
                try {
                    bitmap = mIzkcService.createQRCode(codigo, 500, 500);
                } catch (RemoteException e) {
                    e.printStackTrace();
                }
            } else {
                try {
                    bitmap = mIzkcService.createBarCode(codigo, 1, 1000, 400, false);
                } catch (RemoteException e) {
                    e.printStackTrace();
                }
            }
            if (bitmap != null) {
                try {
                    mIzkcService.printImageGray(bitmap);
                    if (bitmap_foto != null) {
                        mIzkcService.printRasterImage(bitmap_foto);
//                    mIzkcService.printBitmapAlgin(bitmap_foto, 600, 400, 1);
                    }
                } catch (RemoteException e) {
                    e.printStackTrace();
                }
            }
            try {
                if (mIzkcService != null && mIzkcService.checkPrinterAvailable()) {
                    mIzkcService.printerInit();
                    mIzkcService.printGBKText("Patente: " + transaccion.getPatente() + "\n"
                            + "Codigo: " + codigo + "\n"
                            + "Fecha ingreso: " + transaccion.getFechaIngreso() + "\n"
                            + "Hora ingreso: " + transaccion.getHoraIngreso() + "\n"
                            + "\n"
                            + "\n"
                            + "\n"
                            + "\n"
                    );
                    mIzkcService.generateSpace();
                }
            } catch (RemoteException e) {
                e.printStackTrace();
            }

            try {
                mIzkcService.stopRunningTask();
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
    }

    public void capturarImagen() {
        imagenBase64 = "";
        bitmap_foto = null;
        Intent camaraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(camaraIntent, REQUEST_CODE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        imagenBase64 = "";
        if (requestCode == REQUEST_CODE && resultCode == RESULT_OK) {
            bitmap_foto = null;
            Bitmap bitmap = data.getParcelableExtra("data");
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream);
            bitmap_foto = bitmap;
            byte[] byteArray = byteArrayOutputStream.toByteArray();
            String encoded = Base64.encodeToString(byteArray, Base64.DEFAULT);
//            reconocerPatente(encoded);
            imagenBase64 = encoded;
            Button btn = findViewById(R.id.btn_foto);
            btn.setText("Foto almacenada");
//            ImageView im = findViewById(R.id.img_captura);
//            im.setImageBitmap(bitmap);
        }
        if (requestCode == 5) {
            if (resultCode == RESULT_OK) {
                String escaneo = data.getData().toString();
                enviarFragment(escaneo);
            }
        }
    }

    private void cerrarTeclado() {
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            assert imm != null;
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    ///////////////////////////
    // METODOS PARA DESPACHO //
    ///////////////////////////
    public void leerCodigo(final View vistaRoot) {
        cerrarTeclado();
        leerConfiguracion();
        EditText edt_despacho = vistaRoot.findViewById(R.id.edt_despacho);
        final TextView txt_patenteDespacho = vistaRoot.findViewById(R.id.txt_patenteDespacho);
        final TextView txt_fechaIngreso = vistaRoot.findViewById(R.id.txt_fechaIngresoDespacho);
        final TextView txt_horaIngresoD = vistaRoot.findViewById(R.id.txt_horaIngresoDespacho);
        final TextView txt_tiempoTranscurrido = vistaRoot.findViewById(R.id.txt_tiempoTranscurrido);
        final TextView txt_totalCobro = vistaRoot.findViewById(R.id.txt_totalCobro);
        final CardView cardView = vistaRoot.findViewById(R.id.card_despacho);
        cardView.setVisibility(View.GONE);
        txt_patenteDespacho.setText("");
        txt_fechaIngreso.setText("");
        txt_horaIngresoD.setText("");
        txt_tiempoTranscurrido.setText("");
        txt_totalCobro.setText("");
        ImageView imageView = vistaRoot.findViewById(R.id.img_despacho);
        imageView.setImageResource(0);
        escaneado = edt_despacho.getText().toString().trim();
        if (!TextUtils.isEmpty(escaneado)) {
            if (escaneado.length() == 13) {
                String fecha = escaneado.substring(0, 8);
                Log.d(TAG, "FECHA: "+fecha + " ESCANEADO "+escaneado);
                Query query = database.getReference().child(Constant.NODO_FIREBASE).child(fecha).child(escaneado);
                query.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        limpiarCamposDespacho();
                        if (dataSnapshot.exists()) {
                            Transaccion transaccion = dataSnapshot.getValue(Transaccion.class);
                            if (transaccion.getEstado() == 0) {
                                try {
                                    txt_patenteDespacho.append(transaccion.getPatente());
                                    txt_fechaIngreso.append(transaccion.getFechaIngreso());
                                    txt_horaIngresoD.append(transaccion.getHoraIngreso());
                                    //AQUI CALCULAR LA TARIFA
                                    transaccion.setId(escaneado);
                                    calcularTarifaWS(vistaRoot, transaccion);
                                } catch (Exception e) {
                                    Log.d(TAG, "ERROR CATCH: " + e.getMessage());
                                }
                            } else {
                                mostrarSnack(0, "Vehículo ya despachado");
                                try{
                                    EditText editText = findViewById(R.id.edt_despacho);
                                    editText.setText("");
                                }catch (Exception e){
                                    e.printStackTrace();
                                }
                                limpiarCamposDespacho();
                            }
                        } else {
                            mostrarSnack(1, "El registro no existe");
                            EditText editText = findViewById(R.id.edt_despacho);
                            editText.setText("");
                            limpiarCamposDespacho();
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });
            } else {
                limpiarCamposDespacho();
            }
        }
    }

    public void escaneoManual(View view) {
        EditText editText = findViewById(R.id.edt_despacho);
        editText.setText("");
        limpiarCamposDespacho();
        if (modo_telefono) {
            startActivityForResult(new Intent(Inicio.this, Escaner.class), 5);
        } else {
            try {
                if (mIzkcService != null) {
                    mIzkcService.scan();
                }
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
    }

    private void calcularTarifaWS(final View vistaRoot, final Transaccion transaccion) {
        limpiarCamposDespacho();
        //Inicializar Volley
        volleySingleton = VolleySingleton.getInstance(getApplicationContext().getApplicationContext());
        requestQueue = volleySingleton.getmRequestQueue();
        JSONObject dataEnvio = new JSONObject();
        try {
            JSONObject padre = new JSONObject();
            padre.put("id", transaccion.getId());
            padre.put("estado", transaccion.getEstado());
            padre.put("patente", transaccion.getPatente());
            padre.put("fechaIngreso", transaccion.getFechaIngreso());
            padre.put("horaIngreso", transaccion.getHoraIngreso());
            padre.put("imagen", transaccion.getImagen());
            dataEnvio.put("trans", padre);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        SharedPreferences pref = Inicio.this.getSharedPreferences(getString(R.string.PREF), Context.MODE_PRIVATE);
        String ip_server = pref.getString("ip_server", "");
        Log.d(TAG, "IP : " + ip_server);
        if (ip_server.length() > 0) {
            JsonObjectRequest peticion = new JsonObjectRequest(Request.Method.POST, "http://" + ip_server + Constant.URL_CALCULAR_TARIFA, dataEnvio, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    limpiarCamposDespacho();
                    Log.d(TAG, "onResponse: " + response.toString());
                    JSONObject padre;
                    try {
                        padre = response.getJSONObject("CalcularTarifaResult");
                        tar_estado = padre.optBoolean("estado");
                        tar_mensaje = padre.optString("mensaje");
                        tar_fecha_entrada = padre.optString("fecha_entrada");
                        tar_hora_entrada = padre.optString("hora_entrada");
                        tar_fecha_salida = padre.optString("fecha_salida");
                        tar_hora_salida = padre.optString("hora_salida");
                        tar_id_transaccion = padre.optString("id_transaccion");
                        tar_patente = padre.optString("patente");
                        tar_total_cobro = padre.getInt("total_cobro");
                        tar_tiempo_transcurrido = padre.getString("tiempo_transcurrido");
                        tar_imagen = padre.getString("imagen");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    if (tar_estado) {
                        if (tar_mensaje.equals("OK")) {
                            try {
                                final TextView txt_patenteDespacho = findViewById(R.id.txt_patenteDespacho);
                                final TextView txt_fechaIngreso = findViewById(R.id.txt_fechaIngresoDespacho);
                                final TextView txt_horaIngresoD = findViewById(R.id.txt_horaIngresoDespacho);
                                final TextView txt_tiempoTranscurrido = findViewById(R.id.txt_tiempoTranscurrido);
                                final TextView txt_totalCobro = findViewById(R.id.txt_totalCobro);
                                txt_patenteDespacho.append(tar_patente);
                                txt_fechaIngreso.append(tar_fecha_entrada);
                                txt_horaIngresoD.append(tar_hora_entrada);
                                txt_tiempoTranscurrido.append(tar_tiempo_transcurrido);
                                txt_totalCobro.append("$ " + tar_total_cobro);
                                if (transaccion.getImagen() != null) {
                                    byte[] decodedString = Base64.decode(transaccion.getImagen(), Base64.DEFAULT);
                                    Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                                    ImageView imageView = vistaRoot.findViewById(R.id.img_despacho);
                                    imageView.setImageBitmap(decodedByte);
                                }
                                CardView cardView = findViewById(R.id.card_despacho);
                                cardView.setVisibility(View.VISIBLE);
                            } catch (Exception e) {
                                Log.d(TAG, "Error: " + e);
                            }
                        }
                    } else {
                        mostrarSnack(1, "Error al calcular total");
                        CardView cardView = findViewById(R.id.card_despacho);
                        cardView.setVisibility(View.GONE);
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.d(TAG, "ERROR VOLLEY " + error.toString());
                    try {
                        CardView cardView = findViewById(R.id.card_despacho);
                        cardView.setVisibility(View.GONE);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    NetworkResponse response = error.networkResponse;
                    if (error instanceof NetworkError) {

                    } else if (error instanceof ServerError) {
                        String res = null;
                        try {
                            res = new String(response.data, HttpHeaderParser.parseCharset(response.headers, "utf-8"));
                            Log.d(TAG, "onErrorResponse: " + res);
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }
                        Log.e(TAG, "Server Error: " + error.networkResponse.statusCode);
                    } else if (error instanceof AuthFailureError) {
                        Log.e(TAG, "AuthFailureError: " + error.getMessage());
                    } else if (error instanceof ParseError) {
                        Log.e(TAG, "ParseError: " + error.getMessage());
                    } else if (error instanceof TimeoutError) {
                        Log.e(TAG, "TimeoutError " + error.getMessage());
                    }
                    mostrarSnack(2, "Error en servidor al calcular tarifa");
                }
            }) {
                @Override
                public Map<String, String> getHeaders() {
                    Map<String, String> cabeceras = new Hashtable<>();
                    cabeceras.put("Content-Type", "application/json; charset=utf-8");
                    return cabeceras;
                }
            };
            RequestQueue requestQueue = volleySingleton.getmRequestQueue();
            requestQueue.add(peticion);
        } else {
            mostrarSnack(2, "Ip del servidor no definida en configuración");
        }
    }

    public void limpiarCamposDespacho() {
        try {
            CardView cardView = findViewById(R.id.card_despacho);
            cardView.setVisibility(View.GONE);
            TextView txt_patenteDespacho = findViewById(R.id.txt_patenteDespacho);
            TextView txt_fechaIngreso = findViewById(R.id.txt_fechaIngresoDespacho);
            TextView txt_horaIngresoD = findViewById(R.id.txt_horaIngresoDespacho);
            TextView txt_tiempoTranscurrido = findViewById(R.id.txt_tiempoTranscurrido);
            TextView txt_totalCobro = findViewById(R.id.txt_totalCobro);
            txt_patenteDespacho.setText("");
            txt_fechaIngreso.setText("");
            txt_horaIngresoD.setText("");
            txt_tiempoTranscurrido.setText("");
            txt_totalCobro.setText("");
            ImageView imageView = findViewById(R.id.img_despacho);
            imageView.setImageResource(0);

        } catch (Exception e) {
            Log.d(TAG, "limpiarCamposDespacho: " + e);
        }
    }

    public void generarDespacho() {
        cerrarTeclado();
        leerConfiguracion();
        EditText edt_despacho = findViewById(R.id.edt_despacho);
        String id = edt_despacho.getText().toString();
        if (id.length() > 0) {
            Transaccion transUpd = new Transaccion();
            if (tar_id_transaccion != null && tar_estado) {
                transUpd.setId(tar_id_transaccion);
                transUpd.setEstado(1);
                transUpd.setPatente(tar_patente);
                transUpd.setFechaIngreso(tar_fecha_entrada);
                transUpd.setHoraIngreso(tar_hora_entrada);
                transUpd.setFechaSalida(tar_fecha_salida);
                transUpd.setHoraSalida(tar_hora_salida);
                transUpd.setPago(tar_total_cobro);
                transUpd.setImagen(tar_imagen);
            }
            DatabaseReference referenciaDB = database.getReference().child(Constant.NODO_FIREBASE).child(transUpd.getFechaIngreso().replace("-","")).child(id);
            referenciaDB.setValue(transUpd);
            CardView cardView = findViewById(R.id.card_despacho);
            cardView.setVisibility(View.GONE);
            bitmap_foto = null;
            imagenBase64 = "";
            fragment = new Despacho();
            if (imprime_despacho) {
                imprimirComprobante(transUpd);
            }
        } else {
            mostrarSnack(1, "Ingrese un código");
        }
    }

    public void imprimirComprobante(Transaccion transaccion) {
        if (DEVICE_MODEL > 0) {
            String codigo = Funciones.agregaCerosIzq(String.valueOf(transaccion.getId()), 7);
            String limpio = transaccion.getPatente().replace("\n", "").replace("\r", "");
            try {
                if (mIzkcService != null && mIzkcService.checkPrinterAvailable()) {
                    mIzkcService.printGBKText("Patente: " + transaccion.getPatente() + "\n"
                            + "Fecha ingreso: " + transaccion.getFechaIngreso() + "\n"
                            + "Hora ingreso: " + transaccion.getHoraIngreso() + "\n"
                            + "Fecha salida: " + transaccion.getFechaSalida() + "\n"
                            + "Hora salida: " + transaccion.getHoraSalida() + "\n"
                            + "Total a pagar: $" + transaccion.getPago() + "\n"
                            + "\n"
                            + "\n"
                            + "\n"
                            + "\n"
                            + "\n"
                    );
                }
            } catch (RemoteException e) {
                e.printStackTrace();
            }

            try {
                mIzkcService.stopRunningTask();
            } catch (RemoteException e) {
                e.printStackTrace();
            }
            limpiarCamposDespacho();
        }
    }

    @Override
    public boolean dispatchKeyEvent(KeyEvent event) {
        if (event.getKeyCode() == 135 && DEVICE_MODEL > 0) {
            cerrarTeclado();
            if (event.getAction() == KeyEvent.ACTION_UP) {
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                assert imm != null;
                imm.toggleSoftInput(0, InputMethodManager.HIDE_NOT_ALWAYS);
                module_flag = getIntent().getIntExtra(MODULE_FLAG, 4);
                bindService();
                try {
                    if (mIzkcService != null) {
                        mIzkcService.scan();
                    }
                } catch (RemoteException e) {
                    e.printStackTrace();
                }
                return true;
            }
        }
//        if (event.getKeyCode() == KeyEvent.KEYCODE_ENTER) {
//            if (event.getAction() == KeyEvent.ACTION_UP) {
//                Log.d(TAG, "FRAGMENT : " + fragment.toString());
//                cerrarTeclado();
//                if (fragment instanceof Ingreso) {
//                    ingresarVehiculo();
//                    return false;
//                }
//                if (fragment instanceof Despacho) {
//                    generarDespacho();
//                    return false;
//                }
//            }
//        }

//        if (event.getKeyCode() == KeyEvent.KEYCODE_ENTER && fragment instanceof Ingreso) {
//            Log.d(TAG, "ENTER "+fragment.toString());
//            ingresarVehiculo();
//            return true;
//        }
//        if (event.getKeyCode() == KeyEvent.KEYCODE_ENTER && fragment instanceof Despacho) {
//            Log.d(TAG, "ENTER "+fragment.toString());
//            generarDespacho();
//            return true;
//        }
        if (event.getKeyCode() == 123) {
            startActivityForResult(new Intent(Inicio.this, Escaner.class), 5);
            return true;
        }
        if (event.getKeyCode() == 19) {
            if (event.getAction() == KeyEvent.ACTION_UP) {
                cambiarFragment(1);
                return true;
            }
        }
        if (event.getKeyCode() == 21) {
            if (event.getAction() == KeyEvent.ACTION_UP) {
                cambiarFragment(2);
                return true;
            }
        }
        if (event.getKeyCode() == 22) {
            if (event.getAction() == KeyEvent.ACTION_UP) {
                cambiarFragment(3);
                return true;
            }
        }
        if (event.getKeyCode() == 20) {
            if (event.getAction() == KeyEvent.ACTION_UP) {
                cambiarFragment(4);
                return true;
            }
        }
        return super.dispatchKeyEvent(event);
    }

    private void cambiarFragment(int tipo) {
        leerConfiguracion();
        String titulo = null;
        boolean FragmentTransaction = false;
        switch (tipo) {
            case 1:
                fragment = new Home();
                titulo = "Inicio";
                FragmentTransaction = true;
                break;
            case 2:
                if (menu_ingreso) {
                    fragment = new Ingreso();
                    titulo = "Ingreso automóvil";
                    FragmentTransaction = true;
                } else {
                    mostrarSnack(1, "Ingreso desactivado en configuración");
                    FragmentTransaction = false;
                }
                break;
            case 3:
                if (menu_despacho) {
                    fragment = new Despacho();
                    titulo = "Despacho automóvil";
                    FragmentTransaction = true;
                } else {
                    mostrarSnack(1, "Despacho desactivado en configuración");
                    FragmentTransaction = false;
                }
                break;
            case 4:
                fragment = new Configuracion();
                titulo = "Configuración";
                FragmentTransaction = true;
                break;
            default:
                fragment = new Home();
                break;
        }
        if (FragmentTransaction) {
            getSupportFragmentManager().beginTransaction().replace(R.id.contenedor_fragments, fragment).commit();
            Objects.requireNonNull(getSupportActionBar()).setTitle(titulo);
        }
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
    }

    /////////////////////////
    // METODOS DE LA CLASE //
    /////////////////////////
//    @Override
//    public void onBackPressed() {
//        DrawerLayout drawer = findViewById(R.id.drawer_layout);
//        if (drawer.isDrawerOpen(GravityCompat.START)) {
//            drawer.closeDrawer(GravityCompat.START);
//        } else {
//            super.onBackPressed();
//        }
//    }

    @Override
    protected void onDestroy() {
        try {
            if (mIzkcService != null) {
                mIzkcService.unregisterCallBack("Scanner", mCallback);
            }
        } catch (RemoteException e) {
            e.printStackTrace();
        }
        super.onDestroy();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.inicio, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            default:
                break;
        }
        super.onOptionsItemSelected(item);
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        leerConfiguracion();
        int id = item.getItemId();
        boolean FragmentTransaction = false;
        if (id == R.id.nav_inicio) {
            fragment = new Home();
            FragmentTransaction = true;
        } else if (id == R.id.nav_ingreso) {
            fragment = new Ingreso();
            FragmentTransaction = true;
        } else if (id == R.id.nav_despacho) {
            fragment = new Despacho();
            FragmentTransaction = true;
        } else if (id == R.id.nav_configurar) {
            fragment = new Configuracion();
            FragmentTransaction = true;
        }
        if (FragmentTransaction) {
            getSupportFragmentManager().beginTransaction().replace(R.id.contenedor_fragments, fragment).commit();
            item.setChecked(true);
            Objects.requireNonNull(getSupportActionBar()).setTitle(item.getTitle());
        }
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void mostrarSnack(int tipo, String mensaje) {
        Snackbar snackbar = Snackbar.make(vista, mensaje, Snackbar.LENGTH_LONG);
        View sbView = snackbar.getView();
        switch (tipo) {
            case 0:
                sbView.setBackgroundColor(ContextCompat.getColor(Inicio.this, R.color.verde));
                break;
            case 1:
                sbView.setBackgroundColor(ContextCompat.getColor(Inicio.this, R.color.amarillo));
                break;
            case 2:
                sbView.setBackgroundColor(ContextCompat.getColor(Inicio.this, R.color.rojo));
                break;
        }
        snackbar.show();
    }

    @Override
    protected void onResume() {
        super.onResume();
        leerConfiguracion();
//        inciarLectorBarras();
    }

    ///////////////////////////////
    // PARA IMPRESORA Y ESCANER //
    //////////////////////////////
    private void inciarLectorBarras() {
        if (!modo_telefono) {
            //        BaseActivity.module_flag = 4;
            ClientConfig.setValue(ClientConfig.OPEN_SCAN, true);
            ClientConfig.setValue(ClientConfig.APPEND_VIBRATE, true);
            ClientConfig.setValue(ClientConfig.APPEND_RINGTONE, true);
            ClientConfig.setValue(ClientConfig.CONTINUE_SCAN, true);
            ClientConfig.setValue(ClientConfig.SCAN_REPEAT, false);
            player = MediaPlayer.create(getApplicationContext(), R.raw.scan);
            vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
        }
    }

    private void registerCallbackAndInitScan() {
        try {
            Log.d(TAG, "INICIAR SCANNER");
            mIzkcService.registerCallBack("Scanner", mCallback);
            mIzkcService.openScan(true);
            mIzkcService.dataAppendEnter(true);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void handleStateMessage(Message message) {
        super.handleStateMessage(message);
        Log.d(TAG, "MANEJO MENSAJE " + message.obj);
        switch (message.what) {
            case MessageType.BaiscMessage.SEVICE_BIND_SUCCESS:
                registerCallbackAndInitScan();
                break;
            case MessageType.BaiscMessage.SEVICE_BIND_FAIL:
                break;
            case MessageType.BaiscMessage.SCAN_RESULT_GET_SUCCESS:
                Log.d(TAG, "ESCANEADO : " + message.obj);
                enviarFragment((String) message.obj);
                break;
            case MessageType.BaiscMessage.DETECT_PRINTER_SUCCESS:
                String msg = (String) message.obj;
                verificaImpresora(msg);
                break;
        }
    }

    private void verificaImpresora(String msg) {
        String status;
        String aidlServiceVersion;
        try {
            status = mIzkcService.getPrinterStatus();
            aidlServiceVersion = mIzkcService.getServiceVersion();
            Log.d(TAG, "ESTADO IMPRESORA: " + status);
            Log.d(TAG, "VERSION SERVICIO: " + aidlServiceVersion);
        } catch (RemoteException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
}