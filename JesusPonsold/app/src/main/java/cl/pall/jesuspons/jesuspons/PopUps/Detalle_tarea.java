package cl.pall.jesuspons.jesuspons.PopUps;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.*;
import cl.pall.jesuspons.jesuspons.BD.DbHelper;
import cl.pall.jesuspons.jesuspons.Modelos.Tarea;
import cl.pall.jesuspons.jesuspons.R;
import cl.pall.jesuspons.jesuspons.Utils.Constant;
import cl.pall.jesuspons.jesuspons.WebServices.VolleySingleton;
import com.android.volley.*;
import com.android.volley.toolbox.JsonObjectRequest;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Hashtable;
import java.util.Map;

public class Detalle_tarea extends AppCompatActivity {
    private static final String TAG = Detalle_tarea.class.getSimpleName();
    private DbHelper db;
    private VolleySingleton volleySingleton;
    public RequestQueue requestQueue;
    private ImageButton btn_cerrar;
    private Button btn_iniciar_tarea;
    private int id_tarea;
    private TextView det_titulo, det_detalle, det_marca, det_modelo, det_vin, det_fecha_crea, det_fecha_exp, det_cierre_negocio, txt_des_estado, det_finalizada;
    //            , det_tiempo, det_iniciada, txt_des_estado, txt_tit_tiempo, det_cierre_negocio;
    private Tarea tarea = new Tarea();
    private int estadoGlog = 0;
    private LocalBroadcastManager broadcaster;
    private static final String INFO_UPDATE_FILTER = "info_update_filter";
    private Date inicio = null;
    private long dias = 0;
    private long horas = 0;
    private long minutos = 0;
    private long segundos = 0;
    private String fechaInicio = null;
    private String horaInicio = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalle_tarea);
        getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);

        db = new DbHelper(Detalle_tarea.this);
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            id_tarea = bundle.getInt("id_tarea");
        }

        btn_cerrar = findViewById(R.id.btn_cerrar);
        btn_iniciar_tarea = findViewById(R.id.btn_iniciar_tarea);
        det_titulo = findViewById(R.id.det_titulo);
        det_detalle = findViewById(R.id.det_detalle);
        det_marca = findViewById(R.id.det_marca);
        det_modelo = findViewById(R.id.det_modelo);
        det_vin = findViewById(R.id.det_vin);
        det_fecha_crea = findViewById(R.id.det_fecha_crea);
        det_fecha_exp = findViewById(R.id.det_fecha_exp);
        txt_des_estado = findViewById(R.id.txt_des_estado);
        det_finalizada = findViewById(R.id.det_finalizada);
//        det_tiempo = findViewById(R.id.det_tiempo);
//        txt_des_estado = findViewById(R.id.txt_des_estado);
//        txt_tit_tiempo = findViewById(R.id.txt_tit_tiempo);
//        det_iniciada = findViewById(R.id.det_iniciada);
        det_cierre_negocio = findViewById(R.id.det_cierre_negocio);
        volleySingleton = VolleySingleton.getInstance(getApplicationContext().getApplicationContext());
        requestQueue = volleySingleton.getmRequestQueue();
        obtenerTarea(id_tarea);
        eventos();
    }

    private void obtenerTarea(int id_tarea) {
        det_titulo.setText("");
        det_detalle.setText("");
        det_marca.setText("");
        det_modelo.setText("");
        det_vin.setText("");
        det_fecha_crea.setText("");
        det_fecha_exp.setText("");
        det_cierre_negocio.setText("");
        if (id_tarea > 0) {
            tarea = db.obtenerTarea(id_tarea);
            det_titulo.setText(tarea.getDetalle());
            det_detalle.setText(tarea.getDetalle());
            det_marca.setText(tarea.getMarca());
            det_modelo.setText(tarea.getModelo());
            det_vin.setText(tarea.getVin());
            det_fecha_crea.setText(tarea.getFecha_crea());
            det_fecha_exp.setText(tarea.getFecha_exp());
            det_cierre_negocio.setText(String.valueOf(tarea.getId_cierre_negocio()));
            estadoGlog = tarea.getEstado();
            switch (tarea.getEstado()) {
                case 1: //Pendiente
                    btn_iniciar_tarea.setText("INICIAR TAREA");
                    txt_des_estado.setVisibility(View.GONE);
                    det_finalizada.setVisibility(View.GONE);
                    btn_iniciar_tarea.setBackground(ResourcesCompat.getDrawable(getResources(), R.drawable.boton_bg_iniciar, null));
                    break;
                case 2: //Iniciarda
                    txt_des_estado.setVisibility(View.GONE);
                    det_finalizada.setVisibility(View.GONE);
                    btn_iniciar_tarea.setText("FINALIZAR TAREA");
                    btn_iniciar_tarea.setBackground(ResourcesCompat.getDrawable(getResources(), R.drawable.boton_bg_finalizar, null));
                    break;
                case 3: //Finalizada
                    txt_des_estado.setVisibility(View.VISIBLE);
                    det_finalizada.setVisibility(View.VISIBLE);
                    det_finalizada.setText(tarea.getFecha_fin());
                    btn_iniciar_tarea.setText("TAREA YA FINALIZADA");
                    btn_iniciar_tarea.setBackground(ResourcesCompat.getDrawable(getResources(), R.drawable.boton_bg_normal, null));
                    break;
                default:
                    break;
            }
        } else {
            finish();
        }
    }

    private void eventos() {
        btn_cerrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        btn_iniciar_tarea.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                iniciarTarea(estadoGlog, tarea);
            }
        });
    }

    private void actualizaWebservice(final Tarea ta) {
        if (ta.getId() > 0 && ta.getCod_ficha_personal() > 0) {
            JSONObject dataEnvio = new JSONObject();
            try {
                JSONObject padre = new JSONObject();
                padre.put("id_accion", ta.getId_accion());
                padre.put("id_cierre_negocio", ta.getId_cierre_negocio());
                padre.put("cod_ficha_personal", ta.getCod_ficha_personal());
                padre.put("responsable", ta.getResponsable());
                padre.put("estado", ta.getEstado());
                padre.put("fecha_crea", ta.getFecha_crea());
                padre.put("hora_crea", ta.getHora_crea());
                padre.put("fecha_exp", ta.getFecha_exp());
                padre.put("hora_exp", ta.getHora_exp());
                padre.put("fecha_ini", ta.getFecha_ini());
                padre.put("hora_ini", ta.getHora_ini());
                padre.put("fecha_fin", ta.getFecha_fin());
                padre.put("hora_fin", ta.getFecha_fin());
                dataEnvio.put("tarea", padre);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            Log.d(TAG, "CLASE: " + dataEnvio.toString());
            JsonObjectRequest peticion = new JsonObjectRequest(Request.Method.POST, Constant.URL_MOD_TAREAS, dataEnvio, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    Log.d(TAG, "RESPUESTA: " + response.toString());
                    boolean estado = false;
                    String mensaje = null;
                    JSONObject padre;
                    try {
                        padre = response.getJSONObject("ModificarTareaResult");
                        mensaje = padre.optString("mensaje");
                        estado = padre.optBoolean("est");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    if (estado) {
                        if (db.modificarTarea(ta)) {
                            Toast.makeText(Detalle_tarea.this, mensaje, Toast.LENGTH_SHORT).show();
                            obtenerTarea(tarea.getId());
                            actualizarInterfaz();
                        } else {
                            Toast.makeText(Detalle_tarea.this, "Error al modificar la tarea local", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(Detalle_tarea.this, mensaje, Toast.LENGTH_SHORT).show();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.d(TAG, "ERROR VOLLEY " + error.toString());
                    if (error instanceof NetworkError) {
                        Log.e(TAG, "Error Network " + error.networkResponse.data[0]);
                    } else if (error instanceof ServerError) {
                        Log.e(TAG, "Server Error: " + error.networkResponse.statusCode);
                    } else if (error instanceof AuthFailureError) {
                        Log.e(TAG, "AuthFailureError: " + error.getMessage());
                    } else if (error instanceof ParseError) {
                        Log.e(TAG, "ParseError: " + error.getMessage());
                    } else if (error instanceof TimeoutError) {
                        Log.e(TAG, "TimeoutError " + error.getMessage());
                    }
                    Toast.makeText(Detalle_tarea.this, "Error en respuesta de servidor", Toast.LENGTH_SHORT).show();
                }
            }) {
                @Override
                public Map<String, String> getHeaders() {
                    Map<String, String> cabeceras = new Hashtable<>();
                    cabeceras.put("Content-Type", "application/json; charset=utf-8");
                    return cabeceras;
                }
            };
            RequestQueue requestQueue = volleySingleton.getmRequestQueue();
            requestQueue.add(peticion);
        }
    }

    private void iniciarTarea(int estado, Tarea tar) {
        switch (estado) {
            case 1: //Pendiente a iniciada
                actualizaTarea(2, tar);
                break;
            case 2: //Iniciada a finalizada
                actualizaTarea(3, tar);
                break;
            case 3:
                Toast.makeText(this, "Esta tarea ya se finalizó", Toast.LENGTH_SHORT).show();
                break;
            default:
                break;
        }
    }

    private void actualizaTarea(int estado, Tarea t) {
        Date date = new Date();
        String fecha = (String) android.text.format.DateFormat.format("dd-MM-yyyy", date.getTime());
        String hora = (String) android.text.format.DateFormat.format("HH:mm:ss", date.getTime());
        if (t.getId() > 0 && estado > 0) {
            Tarea tarUpdate = new Tarea();
            tarUpdate.setId(t.getId());
            tarUpdate.setId_accion(t.getId_accion());
            tarUpdate.setId_cierre_negocio(t.getId_cierre_negocio());
            tarUpdate.setCod_ficha_personal(t.getCod_ficha_personal());
            tarUpdate.setResponsable(t.getResponsable());
            tarUpdate.setEstado(estado);
            tarUpdate.setLectura(1);
            tarUpdate.setDetalle(t.getDetalle());
            tarUpdate.setMarca(t.getMarca());
            tarUpdate.setModelo(t.getModelo());
            tarUpdate.setVin(t.getVin());
            tarUpdate.setFecha_crea(t.getFecha_crea());
            tarUpdate.setHora_crea(t.getHora_crea());
            tarUpdate.setFecha_exp(t.getFecha_exp());
            tarUpdate.setHora_exp(t.getHora_exp());
            if (estado == 2) {
                tarUpdate.setFecha_ini(fecha);
                tarUpdate.setHora_ini(hora);
                tarUpdate.setFecha_fin(null);
                tarUpdate.setHora_fin(null);
            } else if (estado == 3) {
                tarUpdate.setFecha_ini(t.getFecha_ini());
                tarUpdate.setHora_ini(t.getHora_ini());
                tarUpdate.setFecha_fin(fecha);
                tarUpdate.setHora_fin(hora);
            } else {
                tarUpdate.setFecha_ini(null);
                tarUpdate.setHora_ini(null);
                tarUpdate.setFecha_fin(null);
                tarUpdate.setHora_fin(null);
            }
            actualizaWebservice(tarUpdate); //Aqui manda a WS, si es True, actualiza en la app
        }
    }

//    private void actualizaTiempo(Tarea tar) {
//        det_tiempo.setText("");
//        if (tar.getId() > 0) {
//            Date inicioT = null;
//            Date finT = null;
//            String inicioFecha = tar.getFecha_ini();
//            String inicioHora = tar.getHora_ini();
//            String finFecha = tar.getFecha_fin();
//            String finHora = tar.getHora_fin();
//            //Tiempo
//            long segundosEnMilli = 1000;
//            long minutosEnMilli = segundosEnMilli * 60;
//            long horasEnMilli = minutosEnMilli * 60;
//            long diasEnMili = horasEnMilli * 24;
//            SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss");
//            try {
//                inicioT = format.parse(inicioFecha + " " + inicioHora);
//                finT = format.parse(finFecha + " " + finHora);
//                long diferencia = finT.getTime() - inicioT.getTime();
//                dias = diferencia / diasEnMili;
//                diferencia = diferencia % diasEnMili;
//                horas = diferencia / horasEnMilli;
//                diferencia = diferencia % horasEnMilli;
//                minutos = diferencia / minutosEnMilli;
//                diferencia = diferencia % minutosEnMilli;
//                segundos = diferencia / segundosEnMilli;
//                det_tiempo.setText(dias + " días " + horas + " hor " + minutos + " min " + segundos + " seg");
//            } catch (ParseException e) {
//                e.printStackTrace();
//            }
//        }
//    }

//    private void actualizarContador(Tarea tar1) {
//        if (tar1.getId() > 0) {
//            fechaInicio = tar1.getFecha_ini();
//            horaInicio = tar1.getHora_ini();
//            final Handler manejador = new Handler(Looper.getMainLooper());
//            manejador.postDelayed(new Runnable() {
//                @Override
//                public void run() {
//                    long segundosEnMilli = 1000;
//                    long minutosEnMilli = segundosEnMilli * 60;
//                    long horasEnMilli = minutosEnMilli * 60;
//                    long diasEnMili = horasEnMilli * 24;
//                    SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss");
//                    try {
//                        Date date = new Date();
//                        Date actual;
//                        String fechaActual = (String) android.text.format.DateFormat.format("dd-MM-yyyy", date.getTime());
//                        String horaActual = (String) android.text.format.DateFormat.format("HH:mm:ss", date.getTime());
//                        inicio = format.parse(fechaInicio + " " + horaInicio);
//                        actual = format.parse(fechaActual + " " + horaActual);
//                        long diferencia = actual.getTime() - inicio.getTime();
//                        dias = diferencia / diasEnMili;
//                        diferencia = diferencia % diasEnMili;
//                        horas = diferencia / horasEnMilli;
//                        diferencia = diferencia % horasEnMilli;
//                        minutos = diferencia / minutosEnMilli;
//                        diferencia = diferencia % minutosEnMilli;
//                        segundos = diferencia / segundosEnMilli;
//                        det_tiempo.setText(dias + " días " + horas + " hor " + minutos + " min " + segundos + " seg");
//                        if (tarea.getEstado() == 2) {
//                            manejador.postDelayed(this, 1000);
//                        } else {
//                            manejador.removeCallbacks(this);
//                        }
//                    } catch (ParseException e) {
//                        e.printStackTrace();
//                    }
//                }
//            }, 10);
//        }
//    }

    private void actualizarInterfaz() {
        broadcaster = LocalBroadcastManager.getInstance(getBaseContext());
        Intent intent = new Intent(INFO_UPDATE_FILTER);
        intent.putExtra(INFO_UPDATE_FILTER, "");
        broadcaster.sendBroadcast(intent);
    }
}