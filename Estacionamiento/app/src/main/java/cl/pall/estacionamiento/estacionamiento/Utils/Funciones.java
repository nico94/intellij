package cl.pall.estacionamiento.estacionamiento.Utils;
import android.support.test.espresso.core.internal.deps.guava.base.Strings;
import android.util.Log;

import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Funciones {

    public Funciones() {
    }

    public static String quitarCerosIzq(String s) {
        String resp = s.replaceFirst("^0+(?!$)", "");
        return resp;
    }

    public static  String agregaCerosIzq(String s,int n){
        return Strings.padStart(s, n, '0');
    }

    public String formatearRUT(String rut) {
        if (rut == null) {
            return null;
        }
        int cont = 0;
        String format;
        rut = rut.replace(".", "");
        rut = rut.replace("-", "");
        format = "-" + rut.substring(rut.length() - 1);
        for (int i = rut.length() - 2; i >= 0; i--) {
            format = rut.substring(i, i + 1) + format;
            cont++;
            if (cont == 3 && i != 0) {
                format = "." + format;
                cont = 0;
            }
        }
        return format;
    }


    public static String random() {
        Random random = new Random();
        String CHARS = "abcdefghijkmnopqrstuvwxyzABCDEFGHJKLMNOPQRSTUVWXYZ234567890";
        int length = 5;
        StringBuilder token = new StringBuilder(length);
        for (int i = 0; i < length; i++) {
            token.append(CHARS.charAt(random.nextInt(CHARS.length())));
        }
        return token.toString();
    }

    public boolean validaRut(String rut) {
        String rutSinPuntos = rut.replace(".", "");
        Log.d("TAG", "validaRut: rut sin puntos " + rutSinPuntos);
        Pattern pattern = Pattern.compile("^[0-9]+-[0-9kK]{1}$");
        Matcher matcher = pattern.matcher(rutSinPuntos);
        if (!matcher.matches())
            return false;
        String[] stringRut = rutSinPuntos.split("-");
        return stringRut[1].toLowerCase().equals(dv(stringRut[0]));
    }

    public static String dv(String rut) {
        Integer M = 0, S = 1, T = Integer.parseInt(rut);
        for (; T != 0; T = (int) Math.floor(T /= 10))
            S = (S + T % 10 * (9 - M++ % 6)) % 11;
        return (S > 0) ? String.valueOf(S - 1) : "k";
    }
}