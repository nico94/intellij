package cl.pall.jesuspons.jesuspons.Servicios;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

/**
 * Creado por Nicolás en 10/05/2018.
 */
public class ReceiverCall extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        context.startService(new Intent(context, RecibirTareaSocket.class));
    }
}
