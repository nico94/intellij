package com.agro.pall.servicio.Servicios;

import android.Manifest;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import com.agro.pall.servicio.R;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.*;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.*;

import java.text.DateFormat;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

public class GPS_service extends Service implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {
    private static final String TAG = GPS_service.class.getSimpleName();
    private FusedLocationProviderClient mFusedLocationClient;
    private LocationRequest locationRequest;
    private LocationCallback mLocationCallback;
    private FirebaseDatabase database = FirebaseDatabase.getInstance();
    private DatabaseReference referenciaDB = database.getReference("ubicacion");
    private double latitud;
    private double longitud;
    //Setear Timer
    private static final long INTERVALO = 10 * 1000; //60 segundos
    private Timer mTimer;
    private Handler mHandler;

    @Override
    public void onCreate() {
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        if (mTimer != null) {
            mTimer.cancel();
        } else {
            mTimer = new Timer();
        }

        iniciarServicio();
    }

    public void iniciarServicio(){
        Log.d(TAG, "El servicio se ha iniciado");
        ultimaUbicacion();
        crearLocatioRecuest();
        crearCallBack();
        iniciarActualizaciones();
        //mTimer.schedule();
        //mTimer.schedule(new PararServicio(), INTERVALO);
        //mHandler = new Handler();
    }

    class PararServicio extends TimerTask {
        @Override
        public void run() {
            mHandler.post(new Runnable() {
                @Override
                public void run() {
                    pararServicio();
                }
            });
        }
    }

    private void iniciarActualizaciones() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        mFusedLocationClient.requestLocationUpdates(locationRequest, mLocationCallback, null);
    }

    private void crearCallBack() {
        mLocationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                for (Location location : locationResult.getLocations()) {
                    SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences(getString(R.string.FCM_PREF), Context.MODE_PRIVATE);
                    final String token = sharedPreferences.getString(getString(R.string.FCM_TOKEN), "");
                    String lat = String.valueOf(location.getLatitude());
                    String lon = String.valueOf(location.getLongitude());
                    String fecha = DateFormat.getDateTimeInstance().format(new Date());
                    referenciaDB.child("latitud").setValue(lat);
                    referenciaDB.child("longitud").setValue(lon);
                    referenciaDB.child("token").setValue(token);
                    referenciaDB.child("fecha").setValue(fecha);
                    Log.d(TAG, "onLocationResult: FECHA " + fecha + " LAT: " + location.getLatitude() + " LON: " + location.getLongitude() + " TOKEN " + token);
                }
            }
        };
    }

    private void crearLocatioRecuest() {
        locationRequest = LocationRequest.create();
        locationRequest.setInterval(5000);
        locationRequest.setFastestInterval(1000);
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }

    private void ultimaUbicacion() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
          return;
        }
        mFusedLocationClient.getLastLocation()
                .addOnSuccessListener(new OnSuccessListener<Location>() {
                    @Override
                    public void onSuccess(Location location) {
                        if (location != null) {
                            latitud = location.getLatitude();
                            longitud = location.getLongitude();
                            Log.d(TAG, "LAT: " + String.valueOf(latitud) + " LON: " + String.valueOf(longitud));
                        }
                    }
                });
    }

    private void pararServicio(){
        Log.d(TAG, "PARAR SERVICIO");
        mFusedLocationClient.removeLocationUpdates(mLocationCallback);
    }

    @Override
    public int onStartCommand(Intent intent, int flag, int idProcess)    {
        return START_NOT_STICKY;
    }

    @Override
    public void onDestroy() {
        pararServicio();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    //API
    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }
}