package cl.pall.jesuspons.jesuspons.Modelos;

import com.google.gson.annotations.SerializedName;

/**
 * Creado por Nicolás en 16/04/2018.
 */
public class Tarea {
    private int id;
    @SerializedName("id_accion")
    private int id_accion;
    @SerializedName("id_cierre_negocio")
    private int id_cierre_negocio;
    @SerializedName("cod_ficha_personal")
    private int cod_ficha_personal;
    @SerializedName("responsable")
    private String responsable;
    @SerializedName("estado")
    private int estado;
    private int lectura;
    @SerializedName("detalle")
    private String detalle;
    @SerializedName("marca")
    private String marca;
    @SerializedName("modelo")
    private String modelo;
    @SerializedName("vin")
    private String vin;
    @SerializedName("fecha_crea")
    private String fecha_crea;
    @SerializedName("hora_crea")
    private String hora_crea;
    @SerializedName("fecha_exp")
    private String fecha_exp;
    @SerializedName("hora_exp")
    private String hora_exp;
    private String fecha_ini;
    private String hora_ini;
    @SerializedName("fecha_fin")
    private String fecha_fin;
    @SerializedName("hora_fin")
    private String hora_fin;

    public Tarea() {
    }

    public Tarea(int id, int id_accion, int id_cierre_negocio, int cod_ficha_personal, String responsable, int estado, int lectura, String detalle, String marca, String modelo, String vin, String fecha_crea, String hora_crea, String fecha_exp, String hora_exp, String fecha_ini, String hora_ini, String fecha_fin, String hora_fin) {
        this.id = id;
        this.id_accion = id_accion;
        this.id_cierre_negocio = id_cierre_negocio;
        this.cod_ficha_personal = cod_ficha_personal;
        this.responsable = responsable;
        this.estado = estado;
        this.lectura = lectura;
        this.detalle = detalle;
        this.marca = marca;
        this.modelo = modelo;
        this.vin = vin;
        this.fecha_crea = fecha_crea;
        this.hora_crea = hora_crea;
        this.fecha_exp = fecha_exp;
        this.hora_exp = hora_exp;
        this.fecha_ini = fecha_ini;
        this.hora_ini = hora_ini;
        this.fecha_fin = fecha_fin;
        this.hora_fin = hora_fin;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId_accion() {
        return id_accion;
    }

    public void setId_accion(int id_accion) {
        this.id_accion = id_accion;
    }

    public int getId_cierre_negocio() {
        return id_cierre_negocio;
    }

    public void setId_cierre_negocio(int id_cierre_negocio) {
        this.id_cierre_negocio = id_cierre_negocio;
    }

    public int getCod_ficha_personal() {
        return cod_ficha_personal;
    }

    public void setCod_ficha_personal(int cod_ficha_personal) {
        this.cod_ficha_personal = cod_ficha_personal;
    }

    public String getResponsable() {
        return responsable;
    }

    public void setResponsable(String responsable) {
        this.responsable = responsable;
    }

    public int getEstado() {
        return estado;
    }

    public void setEstado(int estado) {
        this.estado = estado;
    }

    public int getLectura() {
        return lectura;
    }

    public void setLectura(int lectura) {
        this.lectura = lectura;
    }

    public String getDetalle() {
        return detalle;
    }

    public void setDetalle(String detalle) {
        this.detalle = detalle;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public String getVin() {
        return vin;
    }

    public void setVin(String vin) {
        this.vin = vin;
    }

    public String getFecha_crea() {
        return fecha_crea;
    }

    public void setFecha_crea(String fecha_crea) {
        this.fecha_crea = fecha_crea;
    }

    public String getHora_crea() {
        return hora_crea;
    }

    public void setHora_crea(String hora_crea) {
        this.hora_crea = hora_crea;
    }

    public String getFecha_exp() {
        return fecha_exp;
    }

    public void setFecha_exp(String fecha_exp) {
        this.fecha_exp = fecha_exp;
    }

    public String getHora_exp() {
        return hora_exp;
    }

    public void setHora_exp(String hora_exp) {
        this.hora_exp = hora_exp;
    }

    public String getFecha_ini() {
        return fecha_ini;
    }

    public void setFecha_ini(String fecha_ini) {
        this.fecha_ini = fecha_ini;
    }

    public String getHora_ini() {
        return hora_ini;
    }

    public void setHora_ini(String hora_ini) {
        this.hora_ini = hora_ini;
    }

    public String getFecha_fin() {
        return fecha_fin;
    }

    public void setFecha_fin(String fecha_fin) {
        this.fecha_fin = fecha_fin;
    }

    public String getHora_fin() {
        return hora_fin;
    }

    public void setHora_fin(String hora_fin) {
        this.hora_fin = hora_fin;
    }
}