package com.track.pall.peopletack.Modelos;

import java.util.List;

public class Usuario {
    private String uId;
    private String idToken;
    private String nombre;
    private String correo;
    private String photoUrl;

    public Usuario() {
    }

    public Usuario(String uId, String idToken, String nombre, String correo, String photoUrl) {
        this.uId = uId;
        this.idToken = idToken;
        this.nombre = nombre;
        this.correo = correo;
        this.photoUrl = photoUrl;
    }

    public String getuId() {
        return uId;
    }

    public void setuId(String uId) {
        this.uId = uId;
    }

    public String getIdToken() {
        return idToken;
    }

    public void setIdToken(String idToken) {
        this.idToken = idToken;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }
}