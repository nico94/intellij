package cl.pall.estacionamiento.estacionamiento.DB;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import cl.pall.estacionamiento.estacionamiento.Modelos.Transaccion;

public class DBController extends SQLiteOpenHelper {
    private static final String DATABASE_NAME = "Estacionamiento";
    private static final int DATABASE_VERSION = 1;
    private static final String TAG = DBController.class.getSimpleName();

    public DBController(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(Sql.sqlCrearTransaccion);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS Transaccion");
        onCreate(db);
    }

    ////////////////////////////
    // METODOS DE TRANSACCION //
    ////////////////////////////
    public int crearTransaccion(Transaccion transaccion) {
        int res = 0;
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("patente", transaccion.getPatente());
        values.put("estado", transaccion.getEstado());
        values.put("fechaIngreso", transaccion.getFechaIngreso());
        values.put("horaIngreso", transaccion.getHoraIngreso());
        values.put("fechaSalida", transaccion.getFechaSalida());
        values.put("horaSalida", transaccion.getHoraSalida());
        values.put("pago", transaccion.getPago());
        values.put("imagen",transaccion.getImagen());
        res = (int) db.insert("Transaccion", null, values);
        db.close();
        Log.d(TAG, "AGREGA INGRESO NRO: " + res +
                " PATENTE: " + transaccion.getPatente() +
                " FECHA: " + transaccion.getFechaIngreso() +
                " HORA: " + transaccion.getHoraIngreso());
        return res;
    }

    public boolean verificaTransaccion(Transaccion transaccion){
        String selectQuery = "SELECT * FROM Transaccion WHERE id = '" + transaccion.getId() + "' AND patente = '" + transaccion.getPatente() + "'";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        int contador = cursor.getCount();
        cursor.close();
        db.close();
        return contador > 0;
    }

    public Transaccion obtenerTransaccion(String id, String patente) {
        Transaccion transaccion = new Transaccion();
        String selectQuery = "SELECT * FROM Transaccion WHERE patente ='" + patente + "' AND id ='" + id + "'";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor != null && cursor.moveToFirst()) {
            transaccion = new Transaccion(
                    cursor.getString(0),
                    cursor.getInt(1),
                    cursor.getString(2),
                    cursor.getString(3),
                    cursor.getString(4),
                    cursor.getString(5),
                    cursor.getString(6),
                    cursor.getInt(7),
                    cursor.getString(8)
            );
            cursor.close();
        }
        return transaccion;
    }

    public boolean modificarTransaccion(Transaccion transaccion) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("patente", transaccion.getPatente());
        values.put("estado", transaccion.getEstado());
        values.put("fechaIngreso", transaccion.getFechaIngreso());
        values.put("horaIngreso", transaccion.getHoraIngreso());
        values.put("fechaSalida", transaccion.getFechaSalida());
        values.put("horaSalida", transaccion.getHoraSalida());
        values.put("pago", transaccion.getPago());
        values.put("imagen", transaccion.getImagen());
        int rows = db.update("Transaccion", values, "id='" + transaccion.getId() + "'", null);
        db.close();
        Log.d(TAG, "MODIFICA INGRESO: " + rows +
                " PATENTE: " + transaccion.getPatente() +
                " ESTADO: " + transaccion.getEstado() +
                " FECHA INGRESO: " + transaccion.getFechaIngreso() +
                " HORA INGRESO: " + transaccion.getHoraIngreso() +
                " FECHA SALIDA: " + transaccion.getFechaSalida() +
                " HORA SALIDA: " + transaccion.getHoraSalida() +
                " PAGO: " + transaccion.getPago());
        return rows > 0;
    }

    public List<Transaccion> listarTransacciones() {
        List<Transaccion> listaTrasaccion = new ArrayList<Transaccion>();
        String selectQuery = "SELECT * FROM Transaccion ORDER BY id DESC";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery,null);
        if(cursor.moveToFirst()){
            do{
                Transaccion transaccion = new Transaccion();
                transaccion.setId(cursor.getString(0));
                transaccion.setEstado(cursor.getInt(1));
                transaccion.setPatente(cursor.getString(2));
                transaccion.setFechaIngreso(cursor.getString(3));
                transaccion.setHoraIngreso(cursor.getString(4));
                transaccion.setFechaSalida(cursor.getString(5));
                transaccion.setHoraSalida(cursor.getString(6));
                transaccion.setPago(cursor.getInt(7));
                transaccion.setImagen(cursor.getString(8));
                listaTrasaccion.add(transaccion);
            }while (cursor.moveToNext());
        }
        db.close();
        return listaTrasaccion;
    }

    public void eliminaTransacciones() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete("Transaccion", null, null);
        db.close();
        Log.d(TAG, "Se elimina la tabla Transaccion");
    }
}
