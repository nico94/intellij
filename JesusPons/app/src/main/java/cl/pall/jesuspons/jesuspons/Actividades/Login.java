package cl.pall.jesuspons.jesuspons.Actividades;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.android.gms.common.util.CrashUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Hashtable;
import java.util.Map;

import cl.pall.jesuspons.jesuspons.BD.DbHelper;
import cl.pall.jesuspons.jesuspons.BD.Sesion;
import cl.pall.jesuspons.jesuspons.Modelos.Usuario;
import cl.pall.jesuspons.jesuspons.R;
import cl.pall.jesuspons.jesuspons.Utils.Constant;
import cl.pall.jesuspons.jesuspons.Utils.Funciones;
import cl.pall.jesuspons.jesuspons.WebServices.VolleySingleton;

public class Login extends AppCompatActivity {
    public final String TAG = Login.class.getSimpleName();
    private EditText txt_usuario, txt_password;
    private TextInputLayout txtUsr, txtPss;
    private Button btn_ingresar;
    private ProgressBar prg_login;
    private View vista;
    private Animation animation;
    private DbHelper db;
    private Sesion sesion;
    private VolleySingleton volleySingleton;
    private RequestQueue requestQueue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        vista = findViewById(android.R.id.content);
        txt_usuario = findViewById(R.id.txt_usuario);
        txt_password = findViewById(R.id.txt_password);
        btn_ingresar = findViewById(R.id.btn_ingreso);
        txtUsr = findViewById(R.id.input_usuario);
        txtPss = findViewById(R.id.input_password);
        prg_login = findViewById(R.id.prg_login);
        prg_login.setVisibility(View.GONE);
        animation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.shake);
        db = new DbHelper(Login.this);
        //Inicializar consultas ws
        volleySingleton = VolleySingleton.getInstance(getApplicationContext().getApplicationContext());
        requestQueue = volleySingleton.getmRequestQueue();
        sesion = new Sesion(Login.this);
        if (sesion.logueado()) {
            startActivity(new Intent(Login.this, Inicio.class));
            finish();
        }
        eventos();
    }

    private void eventos() {
        btn_ingresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                login();
            }
        });
    }

    private void login() {
        prg_login.setVisibility(View.VISIBLE);
        String usuario = txt_usuario.getText().toString().trim();
        String password = txt_password.getText().toString().trim();
        boolean cancelar = false;
        View foco = null;

        //Valida contraseña
        if (TextUtils.isEmpty(password)) {
            Snackbar.make(vista, "Contraseña vacía", Snackbar.LENGTH_LONG).show();
            foco = txt_password;
            txtPss.startAnimation(animation);
            cancelar = true;
        } else if (!passwordValida(password)) {
            Snackbar.make(vista, "La contraseña es demasiado corta", Snackbar.LENGTH_LONG).show();
            foco = txt_password;
            txtPss.startAnimation(animation);
            cancelar = true;
        }
        //Valida usuario
        if (TextUtils.isEmpty(usuario)) {
            Snackbar.make(vista, "El campo de usuario está vacio", Snackbar.LENGTH_LONG).show();
            foco = txt_usuario;
            txtUsr.startAnimation(animation);
            cancelar = true;
        } else if (!usuarioValido(usuario)) {
            Snackbar.make(vista, "El largo del usuario es muy corto", Snackbar.LENGTH_LONG).show();
            foco = txt_usuario;
            txtUsr.startAnimation(animation);
            cancelar = true;
        }

        if (cancelar) {
            prg_login.setVisibility(View.GONE);
            foco.requestFocus();
        } else {
            hacerLogin(usuario, password);
        }
    }

    private void hacerLogin(final String usuario, final String password) {
        if (!TextUtils.isEmpty(usuario) && !TextUtils.isEmpty(password)) {
            JSONObject dataEnvio = new JSONObject();
            try {
                JSONObject padre = new JSONObject();
                padre.put("IdUsuario", usuario);
                padre.put("PswUsuario", Funciones.md5(password));
                dataEnvio.put("usuario", padre);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            JsonObjectRequest peticion = new JsonObjectRequest(Request.Method.POST, Constant.URL_LOGIN, dataEnvio, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    Log.d(TAG, "RESPUESTA: " + response);
                    boolean estado = false;
                    int codigo_usu = 0;
                    int codigo_cargo = 0;
                    String rut = null;
                    String nombres = null;
                    String ap_paterno = null;
                    String ap_materno = null;
                    String fecha_nacimiento = null;
                    String email = null;
                    String telefono = null;
                    String direccion = null;
                    int codigo_comuna = 0;

                    JSONObject padre;
                    try {
                        padre = response.getJSONObject("InciarSesionResult");
                        codigo_usu = padre.optInt("codigo");
                        codigo_cargo = padre.optInt("cod_cargo");
                        rut = padre.optString("rut");
                        nombres = padre.optString("nombres").trim();
                        ap_paterno = padre.optString("ap_paterno").trim();
                        ap_materno = padre.optString("ap_materno").trim();
                        fecha_nacimiento = padre.optString("fecha_nacimiento");
                        email = padre.optString("email");
                        telefono = padre.optString("telefono");
                        direccion = padre.optString("direccion");
                        codigo_comuna = padre.optInt("cod_comuna");
                        estado = padre.optBoolean("estado");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    if (estado) {
                        if (codigo_usu > 0 && codigo_cargo > 0 && codigo_comuna > 0
                                && !TextUtils.isEmpty(usuario) && !TextUtils.isEmpty(password)
                                && !TextUtils.isEmpty(rut) && !TextUtils.isEmpty(nombres)
//                                && !TextUtils.isEmpty(ap_paterno) && !TextUtils.isEmpty(ap_materno)
                                && !TextUtils.isEmpty(fecha_nacimiento)) {
                            Toast.makeText(Login.this, "Bienvenido/a " + nombres +" "+ap_paterno, Toast.LENGTH_SHORT).show();
                            Usuario usu = new Usuario();
                            usu.setCodigo(codigo_usu);
                            usu.setCod_cargo(codigo_cargo);
                            usu.setUsuario(usuario);
                            usu.setPassword(Funciones.md5(password));
                            usu.setRut(rut);
                            usu.setNombres(nombres);
                            usu.setAp_paterno(ap_paterno);
                            usu.setAp_materno(ap_materno);
                            usu.setFecha_nacimiento(fecha_nacimiento);
                            usu.setEmail(email);
                            usu.setTelefono(telefono);
                            usu.setDireccion(direccion);
                            usu.setCod_comuna(codigo_comuna);
                            db.agregaUsuario(usu);
                            sesion.setLogueado(true);
                            mostarHomeUsuario();
                            finish();
                        } else {
                            Snackbar.make(vista, "Datos de usuario no válidos", Snackbar.LENGTH_SHORT).show();
                        }
                    } else {
                        Snackbar.make(vista, "Usuario y/o contraseña incorrectos", Snackbar.LENGTH_SHORT).show();
                        txtUsr.startAnimation(animation);
                        txtPss.startAnimation(animation);
                    }
                    prg_login.setVisibility(View.GONE);
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.d(TAG, "ERROR VOLLEY " + error.toString());
//                    if (error instanceof NetworkError) {
//                        Log.e(TAG, "Error Network " + error.networkResponse.data[0]);
//                    } else if (error instanceof ServerError) {
//                        Log.e(TAG, "Server Error: " + error.networkResponse.statusCode);
//                    } else if (error instanceof AuthFailureError) {
//                        Log.e(TAG, "AuthFailureError: " + error.getMessage());
//                    } else if (error instanceof ParseError) {
//                        Log.e(TAG, "ParseError: " + error.getMessage());
//                    } else if (error instanceof TimeoutError) {
//                        Log.e(TAG, "TimeoutError " + error.getMessage());
//                    }
                    prg_login.setVisibility(View.GONE);
                    Snackbar.make(vista, "Error en conexión al servidor", Snackbar.LENGTH_LONG)
                            .setAction("Reintentar", new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    login();
                                }
                            }).show();
                }
            }) {
                @Override
                public Map<String, String> getHeaders() {
                    Map<String, String> cabeceras = new Hashtable<>();
                    cabeceras.put("Content-Type", "application/json; charset=utf-8");
                    return cabeceras;
                }
            };
            RequestQueue requestQueue = volleySingleton.getmRequestQueue();
            requestQueue.add(peticion);
        }
    }

    private boolean usuarioValido(String usuario) {
        return usuario.length() >= 4;
    }

    private boolean passwordValida(String password) {
        return password.length() >= 4;
    }

    private void mostarHomeUsuario() {
        startActivity(new Intent(this, Inicio.class));
    }
}