package cl.pall.estacionamiento.estacionamiento.Scanner;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.HashMap;

public class ScanManager {
    SharedPreferences pref;
    SharedPreferences.Editor editor;
    Context _context;
    int PRIVATE_MODE = 0;
    private static final String PREFER_NAME = "AndroidExamplePref";
    public static final String KEY_SCAN = "scan";

    public ScanManager(Context _context) {
        this._context = _context;
        pref = _context.getSharedPreferences(PREFER_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }

    //Create login session
    public void crearScan(String scan) {
        editor.putString(KEY_SCAN, scan);
        editor.commit();
    }

    public String getScan() {
        HashMap<String, String> user = new HashMap<String, String>();
        return pref.getString(KEY_SCAN, null);
    }

    public void limpiarScan() {
        // Clearing all user data from Shared Preferences
        editor.clear();
        editor.commit();
    }
}