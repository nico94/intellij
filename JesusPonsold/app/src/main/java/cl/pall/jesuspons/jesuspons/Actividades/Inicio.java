package cl.pall.jesuspons.jesuspons.Actividades;

import android.content.*;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;
import cl.pall.jesuspons.jesuspons.BD.DbHelper;
import cl.pall.jesuspons.jesuspons.BD.Sesion;
import cl.pall.jesuspons.jesuspons.Fragments.Frg_home;
import cl.pall.jesuspons.jesuspons.Fragments.Frg_tareas;
import cl.pall.jesuspons.jesuspons.Modelos.Tarea;
import cl.pall.jesuspons.jesuspons.Modelos.Usuario;
import cl.pall.jesuspons.jesuspons.R;
import cl.pall.jesuspons.jesuspons.Servicios.MiFirebaseMessagingService;
import cl.pall.jesuspons.jesuspons.Utils.Constant;
import cl.pall.jesuspons.jesuspons.WebServices.VolleySingleton;
import com.android.volley.*;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

public class Inicio extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
    private static final String TAG = Inicio.class.getSimpleName();
    private Sesion sesion;
    private DbHelper db;
    private View vista;
    private String token;
    private String nombreDispositivo;
    private NavigationView navigationView;
    //Consultas
    private VolleySingleton volleySingleton;
    private RequestQueue requestQueue;
    private static long back_pressed;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inicio);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        vista = findViewById(android.R.id.content);
        db = new DbHelper(Inicio.this);
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        View header = navigationView.getHeaderView(0);
        Usuario usu = db.obtenerUsuario();
        if (usu.getNombres() != null) {
            TextView txt_nombre_usuario = header.findViewById(R.id.txt_nombre_usuario);
            txt_nombre_usuario.setText(usu.getNombres().trim()+" "+usu.getAp_paterno().trim());
        }
        if (usu.getEmail() != null) {
            TextView txt_correo = header.findViewById(R.id.txt_correo);
            txt_correo.setText(usu.getEmail());
        }
        String new_tarea = getIntent().getStringExtra("tarea");
        if (new_tarea != null) {
            if (new_tarea.equals("new_tarea")) {
                cargaFragmentTareas();
            }
        } else {
            fragmentDefault();
        }
        LocalBroadcastManager.getInstance(this).registerReceiver(actualizar, new IntentFilter(MiFirebaseMessagingService.INFO_UPDATE_FILTER));
        //Incializar Volley
        volleySingleton = VolleySingleton.getInstance(getApplicationContext().getApplicationContext());
        requestQueue = volleySingleton.getmRequestQueue();
        sesion = new Sesion(this);
        if (!sesion.logueado()) {
            finish();
        }
        enviarToken();
        sincronizarTareas(usu.getCodigo(), this);
    }

    /////////////////////////
    // METODOS DE LA CLASE //
    /////////////////////////
    public void sincronizarTareas(int cod_usuario, final Context context) {
        db = new DbHelper(context);
        volleySingleton = VolleySingleton.getInstance(context);
        requestQueue = volleySingleton.getmRequestQueue();
        JSONObject dataEnvio = new JSONObject();
        try {
            dataEnvio.put("cod_ficha_personal", cod_usuario);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        JsonObjectRequest peticion = new JsonObjectRequest(Request.Method.POST, Constant.URL_LIST_TAREAS, dataEnvio, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                GsonBuilder gsonBuilder = new GsonBuilder();
                Gson gson = gsonBuilder.create();
                boolean estado;
                JSONObject padre;
                try {
                    padre = response.getJSONObject("ListarTareasResult");
                    JSONArray json_tareas = padre.getJSONArray("ListadoTareas");
                    estado = padre.optBoolean("estado");
                    if (estado) {
                        Type tareasList = new TypeToken<ArrayList<Tarea>>() {
                        }.getType();
                        List<Tarea> listaTarea = gson.fromJson(json_tareas.toString().trim(), tareasList);
                        new GuardaTareasBD(context).execute(listaTarea);
                    } else {
                        Log.d(TAG, "Error actualizar tareas");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(TAG, "ERROR VOLLEY " + error.toString());
                if (error instanceof NetworkError) {
                    Log.e(TAG, "Error Network " + error.networkResponse.data[0]);
                } else if (error instanceof ServerError) {
                    Log.e(TAG, "Server Error: " + error.networkResponse.statusCode);
                } else if (error instanceof AuthFailureError) {
                    Log.e(TAG, "AuthFailureError: " + error.getMessage());
                } else if (error instanceof ParseError) {
                    Log.e(TAG, "ParseError: " + error.getMessage());
                } else if (error instanceof TimeoutError) {
                    Log.e(TAG, "TimeoutError " + error.getMessage());
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> cabeceras = new Hashtable<>();
                cabeceras.put("Content-Type", "application/json; charset=utf-8");
                return cabeceras;
            }
        };
        RequestQueue requestQueue = volleySingleton.getmRequestQueue();
        requestQueue.add(peticion);
    }

    private void fragmentDefault() {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        Frg_home frg_home = new Frg_home();
        getSupportActionBar().setTitle("Inicio");
        navigationView.getMenu().getItem(0).setChecked(true);
        transaction.replace(R.id.contenedor_fragments, frg_home);
        transaction.commit();
    }

    private void cargaFragmentTareas() {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        Frg_tareas frgtareas = new Frg_tareas();
        getSupportActionBar().setTitle("Tarea");
        navigationView.getMenu().getItem(1).setChecked(true);
        transaction.replace(R.id.contenedor_fragments, frgtareas);
        transaction.commit();
    }

    private void enviarToken() {
        SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences(getString(R.string.FCM_PREF), Context.MODE_PRIVATE);
        token = sharedPreferences.getString(getString(R.string.FCM_TOKEN), "");
        nombreDispositivo = Build.MODEL;
        Usuario usu = db.obtenerUsuario();
        Log.d(TAG, "Enviar Token: " + usu.toString());
        if (usu.getCodigo() > 0) {
            JSONObject dataEnvio = new JSONObject();
            try {
                JSONObject padre = new JSONObject();
                padre.put("des_usuario_token", usu.getCodigo());
                padre.put("des_dispositivo", nombreDispositivo);
                padre.put("des_token", token);
                dataEnvio.put("token", padre);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            JsonObjectRequest peticion = new JsonObjectRequest(Request.Method.POST, Constant.URL_REGISTRO_TOKEN, dataEnvio, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    Log.d(TAG, "RESPUESTA WS: " + response.toString());
                    boolean estado = false;
                    JSONObject padre = null;
                    try {
                        padre = response.getJSONObject("AgregaTokenResult");
                        estado = padre.optBoolean("estado");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    if (estado) {
                        //Toast.makeText(Inicio.this, "TOKEN REGISTRADO", Toast.LENGTH_SHORT).show();
                        Log.d(TAG, "TOKEN REGISTRADO");
                    } else {
                        Log.d(TAG, "TOKEN NO REGISTRADO");
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.d(TAG, "ERROR VOLLEY TOKEN " + error.getMessage());
                    if (error instanceof NetworkError) {
                        Log.e(TAG, "Error Network " + error.networkResponse.data.toString());
                    } else if (error instanceof ServerError) {
                        Log.e(TAG, "Server Error: " + error.networkResponse.statusCode);
                    } else if (error instanceof AuthFailureError) {
                        Log.e(TAG, "AuthFailureError: " + error.getMessage());
                    } else if (error instanceof ParseError) {
                        Log.e(TAG, "ParseError: " + error.getMessage());
                    } else if (error instanceof TimeoutError) {
                        Log.e(TAG, "TimeoutError " + error.getMessage());
                    }
                }
            }) {
                @Override
                public Map<String, String> getHeaders() {
                    Map<String, String> cabeceras = new Hashtable<String, String>();
                    cabeceras.put("Content-Type", "application/json; charset=utf-8");
                    return cabeceras;
                }
            };
            requestQueue = volleySingleton.getmRequestQueue();
            requestQueue.add(peticion);
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            if (back_pressed + 2000 > System.currentTimeMillis()) {
                super.onBackPressed();
            } else {
                Toast.makeText(getBaseContext(), "Vuelva a presionar para salir", Toast.LENGTH_SHORT).show();
                back_pressed = System.currentTimeMillis();
                fragmentDefault();
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_home, menu);
        actualizaContador();
        return true;
    }

    public void actualizaContador() {
        List<Tarea> listTareas = db.obtenerTareas();
        int total = listTareas.size();
        TextView view = (TextView) navigationView.getMenu().findItem(R.id.nav_tareas).getActionView();
        view.setText(total > 0 ? String.valueOf(total) : null);
    }

    private BroadcastReceiver actualizar = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String filtro = MiFirebaseMessagingService.INFO_UPDATE_FILTER;
            if (intent.getAction().equals(filtro)) {
                actualizaContador();
            }
        }
    };

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.limpiar:
                Toast.makeText(this, "Limpia Tarea (testing)", Toast.LENGTH_SHORT).show();
                db.eliminarTareas();
                return true;
            case R.id.cerrar_sesion:
                cerrarSesion();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        Fragment fragment = null;
        if (id == R.id.nav_home) {
            fragment = new Frg_home();
        } else if (id == R.id.nav_tareas) {
            fragment = new Frg_tareas();
        }
        if (fragment != null) {
            transaction.replace(R.id.contenedor_fragments, fragment);
            item.setChecked(true);
            transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
            transaction.commit();
            getSupportActionBar().setTitle(item.getTitle());
        }
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public class GuardaTareasBD extends AsyncTask<List<Tarea>, Integer, String> {
        Context context;
        DbHelper dataBase;

        public GuardaTareasBD(Context context) {
            this.context = context;
            dataBase = new DbHelper(context);
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(List<Tarea>... tareas) {
            List<Tarea> tareaList = tareas[0];
            String salida = null;

            if (tareaList.size() > 0) {
                for (Tarea tar : tareaList) {
                    if (dataBase.verificaExisteTarea(tar)) {
                        if (dataBase.modificarTarea(tar)) {
                            salida = null;
                        } else {
                            salida = "No agregado";
                        }
                    } else {
                        dataBase.agregarTarea(tar);
                    }
                }
            }
            return salida;
        }

        @Override
        protected void onPostExecute(String resultado) {
            super.onPostExecute(resultado);
            if (resultado == null) {
//                cargaFragmentTareas();
            } else {
                Toast.makeText(context, "No se actualizaron las tareas", Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Intent intent = new Intent("cl.jesuspons.jesuspons");
        sendBroadcast(intent);
    }

    private void cerrarSesion() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("¿Está seguro que quiere cerrar sesión?");
        builder.setTitle("Cerrar sesión");
        //builder.setIcon(R.drawable.logout);
        builder.setPositiveButton("Si", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int i) {
                Intent intent = new Intent(Inicio.this, Login.class);
                sesion.setLogueado(false);
                db.eliminarUsuario();
                finish();
                startActivity(intent);
            }
        });
        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int i) {
                dialog.cancel();
            }
        });
        AlertDialog dialogSesion = builder.create();
        dialogSesion.show();
    }
}