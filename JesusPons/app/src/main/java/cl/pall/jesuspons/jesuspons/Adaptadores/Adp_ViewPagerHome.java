package cl.pall.jesuspons.jesuspons.Adaptadores;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import cl.pall.jesuspons.jesuspons.Fragments.Frg_tareas;
import cl.pall.jesuspons.jesuspons.Fragments.Pagina2;
import cl.pall.jesuspons.jesuspons.Fragments.Pagina3;

/**
 * Creado por Nicolás en 16/04/2018.
 */
public class Adp_ViewPagerHome extends FragmentPagerAdapter {
    int numeroDeSecciones;

    public Adp_ViewPagerHome(FragmentManager fm, int numeroDeSecciones) {
        super(fm);
        this.numeroDeSecciones = numeroDeSecciones;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return new Frg_tareas();
            case 1:
                return new Pagina2();
            case 2:
                return new Pagina3();
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return numeroDeSecciones;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return "Tarea";
            case 1:
                return "Concretadas";
            case 2:
                return "Pendientes";
            default:
                return null;
        }
    }
}