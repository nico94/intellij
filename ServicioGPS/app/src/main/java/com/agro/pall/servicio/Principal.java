package com.agro.pall.servicio;

import android.Manifest;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import com.agro.pall.servicio.Servicios.GPS_service;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.firebase.auth.FirebaseAuth;

import java.util.ArrayList;
import java.util.List;

public class Principal extends AppCompatActivity implements
        GoogleApiClient.OnConnectionFailedListener {
    private static final int REQUEST_ID_MULTIPLE_PERMISSIONS = 1;
    private static final String TAG = Principal.class.getSimpleName();
    private static final int RC_SIGN_IN = 1;
    private GoogleApiClient googleApiClient;
    private FirebaseAuth firebaseAuth;
    private FirebaseAuth.AuthStateListener authStateListener;
    SignInButton btn_google;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_principal);

        btn_google = findViewById(R.id.btn_google);
        eventos();
        configurarGoogleAuth();
        varificarPermisos();
        //mostrarIcono();
    }

    private boolean varificarPermisos() {
        if (android.os.Build.VERSION.SDK_INT >= 23) {
            int gps = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION);
            int gps2 = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION);
            List<String> listaPermisos = new ArrayList<>();
            if (gps != PackageManager.PERMISSION_GRANTED) {
                listaPermisos.add(android.Manifest.permission.ACCESS_FINE_LOCATION);
            }
            if (gps2 != PackageManager.PERMISSION_GRANTED) {
                listaPermisos.add(android.Manifest.permission.ACCESS_COARSE_LOCATION);
            }
            if (!listaPermisos.isEmpty()) {
                ActivityCompat.requestPermissions(this, listaPermisos.toArray(new String[listaPermisos.size()]), REQUEST_ID_MULTIPLE_PERMISSIONS);
                return false;
            }
        }
        return true;
    }

    private void ocultarIcono() {
        PackageManager p = getPackageManager();
        ComponentName componentName = new ComponentName(this, Principal.class); // activity which is first time open in manifiest file which is declare as <category android:name="android.intent.category.LAUNCHER" />
        p.setComponentEnabledSetting(componentName,PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
    }

    private void eventos() {
        btn_google.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ocultarIcono();
                loginConGoogle();
            }
        });
    }

    private void loginConGoogle() {
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(googleApiClient);
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult resultado = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            manejarResultado(resultado);
        }
    }

    private void manejarResultado(GoogleSignInResult resultado) {
        if (resultado.isSuccess()) {
            GoogleSignInAccount acct = resultado.getSignInAccount();
            Log.d(TAG, "RES: "+acct.getEmail()+" "+acct.getDisplayName()+" "+acct.getFamilyName());
            if (acct != null) {
                Log.d(TAG, "manejarResultado: "+acct.getEmail());
                if(acct.getEmail()!=null){
                    SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences("correo_pref", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.putString("correo", acct.getEmail());
                    editor.apply();
                    ocultarIcono();
                }
            }else{
                Log.d(TAG, "Error");
            }
        }
    }

    private void configurarGoogleAuth() {
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN).requestEmail().build();
        googleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();

        firebaseAuth = FirebaseAuth.getInstance();
        authStateListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {

            }
        };
    }

    private void iniciarServicio() {
        startService(new Intent(this, GPS_service.class));
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }
}