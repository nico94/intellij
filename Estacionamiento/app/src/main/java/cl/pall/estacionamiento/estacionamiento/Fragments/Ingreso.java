package cl.pall.estacionamiento.estacionamiento.Fragments;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.app.Fragment;
import android.text.InputFilter;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import cl.pall.estacionamiento.estacionamiento.Actividades.Inicio;
import cl.pall.estacionamiento.estacionamiento.R;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Objects;

public class Ingreso extends Fragment {
    static String TAG = Ingreso.class.getSimpleName();
    private View vistaRoot;
    private Button btn_ingreso, btn_foto;
    private EditText edt_patente;
    private TextView txt_hora;
//    private ImageView img_captura;

    public Ingreso() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        vistaRoot = inflater.inflate(R.layout.fragment_ingreso, container, false);
        btn_ingreso = vistaRoot.findViewById(R.id.btn_ingreso_vehiculo);
        edt_patente = vistaRoot.findViewById(R.id.edt_patente);
        txt_hora = vistaRoot.findViewById(R.id.txt_hora);
        btn_foto = vistaRoot.findViewById(R.id.btn_foto);
//        img_captura = vistaRoot.findViewById(R.id.img_captura);
//        img_captura.setImageResource(R.drawable.camara);
        edt_patente.requestFocus();
        eventos();
        return vistaRoot;
    }

    private void eventos() {
        edt_patente.setFilters(new InputFilter[]{new InputFilter.AllCaps(), new InputFilter.LengthFilter(6)});

        edt_patente.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if ((event.getAction() == KeyEvent.ACTION_DOWN) && (keyCode == KeyEvent.KEYCODE_ENTER)) {
                    // Perform action on key press
                    ((Inicio) Objects.requireNonNull(getActivity())).ingresarVehiculo();
                    return true;
                }
                return false;
            }
        });

        final Handler manejador = new Handler(Looper.getMainLooper());
        manejador.postDelayed(new Runnable() {
            @Override
            public void run() {
                @SuppressLint("SimpleDateFormat") SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
                String currentTime = sdf.format(new Date());
                txt_hora.setText(currentTime);
                manejador.postDelayed(this, 1000);
            }
        }, 10);

        btn_ingreso.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((Inicio) Objects.requireNonNull(getActivity())).ingresarVehiculo();
            }
        });

        btn_foto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((Inicio) Objects.requireNonNull(getActivity())).capturarImagen();
            }
        });

//        img_captura.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                ((Inicio) Objects.requireNonNull(getActivity())).capturarImagen();
//            }
//        });
    }
}