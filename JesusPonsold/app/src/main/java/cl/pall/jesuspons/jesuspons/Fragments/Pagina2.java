package cl.pall.jesuspons.jesuspons.Fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import cl.pall.jesuspons.jesuspons.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class Pagina2 extends Fragment {
    View vista;

    public Pagina2() {
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        vista = inflater.inflate(R.layout.fragment_pagina2, container, false);
        return vista;
    }
}