package cl.pall.estacionamiento.estacionamiento.Fragments;

import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import cl.pall.estacionamiento.estacionamiento.Actividades.Inicio;
import cl.pall.estacionamiento.estacionamiento.R;

public class Despacho extends Fragment {
    private View vistaRoot;
    private Bundle datos;
    private String escaneado;
    private EditText edt_despacho;
    private Button btn_despachar, btn_escanear;
    private TextView txt_patenteDespacho, txt_fechaIngreso, txt_horaIngresoD, txt_tiempoTranscurrido, txt_totalCobro;

    public Despacho() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        vistaRoot = inflater.inflate(R.layout.fragment_despacho, container, false);
        datos = this.getArguments();
        if (datos != null) {
            escaneado = datos.getString("escaneado");
        }
        edt_despacho = vistaRoot.findViewById(R.id.edt_despacho);
        btn_despachar = vistaRoot.findViewById(R.id.btn_despachar);
        btn_escanear = vistaRoot.findViewById(R.id.btn_escanear);
        txt_patenteDespacho = vistaRoot.findViewById(R.id.txt_patenteDespacho);
        txt_fechaIngreso = vistaRoot.findViewById(R.id.txt_fechaIngresoDespacho);
        txt_horaIngresoD = vistaRoot.findViewById(R.id.txt_horaIngresoDespacho);
        txt_tiempoTranscurrido = vistaRoot.findViewById(R.id.txt_tiempoTranscurrido);
        txt_totalCobro = vistaRoot.findViewById(R.id.txt_totalCobro);
        edt_despacho.requestFocus();
        if (!TextUtils.isEmpty(escaneado)) {
            edt_despacho.append(escaneado);
        }
        edt_despacho.requestFocus();
        eventos();
        return vistaRoot;
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private void eventos() {
        edt_despacho.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {}

            @Override
            public void onTextChanged(CharSequence charSequence, int start, int despues, int contador) {}

            @Override
            public void afterTextChanged(Editable editable) {
                if (edt_despacho.getText().toString().length() == 13) {
                    ((Inicio) getActivity()).leerCodigo(vistaRoot);
                }
            }
        });
        edt_despacho.setShowSoftInputOnFocus(false);
        edt_despacho.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if((event.getAction()== KeyEvent.ACTION_DOWN) && (keyCode == KeyEvent.KEYCODE_ENTER)){
                    if (edt_despacho.getText().toString().length() == 13) {
                        ((Inicio) getActivity()).generarDespacho();
                    } else {
                        ((Inicio) getActivity()).mostrarSnack(1, "El largo del código no corresponde");
                        ((Inicio) getActivity()).limpiarCamposDespacho();
                    }
                    return true;
                }
                return false;
            }
        });

        edt_despacho.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if (edt_despacho.getText().toString().length() == 13) {
                    ((Inicio) getActivity()).leerCodigo(vistaRoot);
                } else {
                    ((Inicio) getActivity()).limpiarCamposDespacho();
                }
            }
        });

        btn_escanear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Escanear
                ((Inicio) getActivity()).escaneoManual(v);
            }
        });

        btn_despachar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (edt_despacho.getText().toString().matches("")) {
                    ((Inicio) getActivity()).mostrarSnack(1, "Escanee el código a despachar");
                    ((Inicio) getActivity()).limpiarCamposDespacho();
                } else {
                    if (edt_despacho.getText().toString().length() == 13) {
                        ((Inicio) getActivity()).generarDespacho();
                    } else {
                        ((Inicio) getActivity()).mostrarSnack(1, "El largo del código no corresponde");
                        ((Inicio) getActivity()).limpiarCamposDespacho();
                    }
                }
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        edt_despacho.setText(escaneado);
    }
}