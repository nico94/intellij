package cl.pall.estacionamiento.estacionamiento.DB;

public class Sql {
    // CREAR TABLAS
    static String sqlCrearTransaccion = "CREATE TABLE Transaccion (" +
            "id INTEGER PRIMARY KEY AUTOINCREMENT, " +
            "estado INTEGER, "+
            "patente TEXT, " +
            "fechaIngreso TEXT, " +
            "horaIngreso TEXT, " +
            "fechaSalida TEXT, " +
            "horaSalida TEXT, " +
            "pago INTEGER, " +
            "imagen TEXT)";
}