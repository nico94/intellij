package cl.pall.jesuspons.jesuspons.Modelos;

/**
 * Creado por Nicolás en 13/04/2018.
 */
public class Usuario {
    private int id;
    private int codigo;
    private int cod_cargo;
    private String usuario;
    private String password;
    private String rut;
    private String nombres;
    private String ap_paterno;
    private String ap_materno;
    private String fecha_nacimiento;
    private String email;
    private String telefono;
    private String direccion;
    private int cod_comuna;

    public Usuario() {
    }

    public Usuario(int id, int codigo, int cod_cargo, String usuario, String password, String rut, String nombres, String ap_paterno, String ap_materno, String fecha_nacimiento, String email, String telefono, String direccion, int cod_comuna) {
        this.id = id;
        this.codigo = codigo;
        this.cod_cargo = cod_cargo;
        this.usuario = usuario;
        this.password = password;
        this.rut = rut;
        this.nombres = nombres;
        this.ap_paterno = ap_paterno;
        this.ap_materno = ap_materno;
        this.fecha_nacimiento = fecha_nacimiento;
        this.email = email;
        this.telefono = telefono;
        this.direccion = direccion;
        this.cod_comuna = cod_comuna;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public int getCod_cargo() {
        return cod_cargo;
    }

    public void setCod_cargo(int cod_cargo) {
        this.cod_cargo = cod_cargo;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRut() {
        return rut;
    }

    public void setRut(String rut) {
        this.rut = rut;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getAp_paterno() {
        return ap_paterno;
    }

    public void setAp_paterno(String ap_paterno) {
        this.ap_paterno = ap_paterno;
    }

    public String getAp_materno() {
        return ap_materno;
    }

    public void setAp_materno(String ap_materno) {
        this.ap_materno = ap_materno;
    }

    public String getFecha_nacimiento() {
        return fecha_nacimiento;
    }

    public void setFecha_nacimiento(String fecha_nacimiento) {
        this.fecha_nacimiento = fecha_nacimiento;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public int getCod_comuna() {
        return cod_comuna;
    }

    public void setCod_comuna(int cod_comuna) {
        this.cod_comuna = cod_comuna;
    }
}