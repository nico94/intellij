package cl.pall.estacionamiento.estacionamiento.Modelos;

import com.google.gson.annotations.SerializedName;

/**
 * Creado por Nicolás en 30/08/2018.
 */

public class Transaccion {
    @SerializedName("id")
    private String id;
    @SerializedName("estado")
    private int estado;
    @SerializedName("patente")
    private String patente;
    @SerializedName("fechaIngreso")
    private String fechaIngreso;
    @SerializedName("horaIngreso")
    private String horaIngreso;
    @SerializedName("fechaSalida")
    private String fechaSalida;
    @SerializedName("horaSalida")
    private String horaSalida;
    @SerializedName("pago")
    private int pago;
    @SerializedName("imagen")
    private String imagen;

    public Transaccion() {
    }

    public Transaccion(String id, int estado, String patente, String fechaIngreso, String horaIngreso, String fechaSalida, String horaSalida, int pago, String imagen) {
        this.id = id;
        this.estado = estado;
        this.patente = patente;
        this.fechaIngreso = fechaIngreso;
        this.horaIngreso = horaIngreso;
        this.fechaSalida = fechaSalida;
        this.horaSalida = horaSalida;
        this.pago = pago;
        this.imagen = imagen;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getEstado() {
        return estado;
    }

    public void setEstado(int estado) {
        this.estado = estado;
    }

    public String getPatente() {
        return patente;
    }

    public void setPatente(String patente) {
        this.patente = patente;
    }

    public String getFechaIngreso() {
        return fechaIngreso;
    }

    public void setFechaIngreso(String fechaIngreso) {
        this.fechaIngreso = fechaIngreso;
    }

    public String getHoraIngreso() {
        return horaIngreso;
    }

    public void setHoraIngreso(String horaIngreso) {
        this.horaIngreso = horaIngreso;
    }

    public String getFechaSalida() {
        return fechaSalida;
    }

    public void setFechaSalida(String fechaSalida) {
        this.fechaSalida = fechaSalida;
    }

    public String getHoraSalida() {
        return horaSalida;
    }

    public void setHoraSalida(String horaSalida) {
        this.horaSalida = horaSalida;
    }

    public int getPago() {
        return pago;
    }

    public void setPago(int pago) {
        this.pago = pago;
    }

    public String getImagen() {
        return imagen;
    }

    public void setImagen(String imagen) {
        this.imagen = imagen;
    }
}