package cl.pall.jesuspons.jesuspons.Utils;

/**
 * Creado por Nicolás en 13/04/2018.
 */
public class Constant {
    /////////////////////////
    // VARIABLES GLPOBALES //
    /////////////////////////
    //private static String IP = "http://192.168.50.100";
    private static String IP = "http://186.10.19.170";
    //    private static String IP = "http://192.168.50.218";
    private static String WEB_SERVICE = IP + "/wsJesusPons/";
    private static String SVC_USUARIO = WEB_SERVICE + "Servicios/Usuario.svc/rest/";
    private static String SVC_TOKENS = WEB_SERVICE + "Servicios/Tokens.svc/rest/";
    private static String SVC_TAREAS = WEB_SERVICE + "Servicios/Tareas.svc/rest/";

    //////////
    // APIS //
    //////////
    public static String URL_LOGIN = SVC_USUARIO + "login";
    public static String URL_REGISTRO_TOKEN = SVC_TOKENS + "reg_token";
    public static String URL_LIST_TAREAS = SVC_TAREAS + "listar_tareas";
    public static String URL_MOD_TAREAS = SVC_TAREAS + "modificar_tarea";

    //public static String URL_LOGIN = "http://" + IP + "/wsJesusPons/Servicios/RegistraToken.svc/login";
    //public static String URL_REGISTRO_TOKEN = "http://" + IP + "/wsJesusPons/Servicios/RegistraToken.svc/reg_token";
}