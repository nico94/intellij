package cl.pall.jesuspons.jesuspons.Fragments;


import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;


import com.github.mikephil.charting.charts.*;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.github.mikephil.charting.utils.ColorTemplate;

import java.util.ArrayList;
import java.util.List;

import cl.pall.jesuspons.jesuspons.BD.DbHelper;
import cl.pall.jesuspons.jesuspons.R;

public class Frg_home extends Fragment {
    private final static String TAG = Frg_home.class.getSimpleName();
    View vista;
    PieChart grafico;
    DbHelper db;

    public Frg_home() {
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        vista = inflater.inflate(R.layout.fragment_frg_home, container, false);
        grafico = vista.findViewById(R.id.grafico);
        db = new DbHelper(getActivity());
        llenarGrafico();
        return vista;
    }

    private void llenarGrafico() {
        int pendientes = db.totalTareasPorEstado(1);
        int iniciadas = db.totalTareasPorEstado(2);
        int finalizadas = db.totalTareasPorEstado(3);

        List<Entry> yvalues = new ArrayList<>();
        yvalues.add(new Entry(pendientes, 0));
        yvalues.add(new Entry(iniciadas, 1));
        yvalues.add(new Entry(finalizadas, 2));

        PieDataSet dataSet = new PieDataSet(yvalues, "");
        dataSet.setColors(ColorTemplate.JOYFUL_COLORS);

        ArrayList<String> xVals = new ArrayList<>();

        xVals.add("Pendientes");
        xVals.add("Iniciadas");
        xVals.add("Finalizadas");

        PieData data = new PieData(xVals, dataSet);
        data.setValueTextSize(13f);
        data.setValueTextColor(Color.DKGRAY);
        grafico.setDrawHoleEnabled(false);
        grafico.setData(data);
        grafico.animateXY(1400, 1400);
        grafico.setOnChartValueSelectedListener(new OnChartValueSelectedListener() {
            @Override
            public void onValueSelected(Entry e, int dataSetIndex, Highlight h) {
                if (e == null)
                    return;
//                Toast.makeText(getActivity(), " Seleccion: " + e.getVal() + " Indice: " + e.getXIndex() , Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onNothingSelected() {

            }
        });
    }
}
