package cl.pall.jesuspons.jesuspons.BD;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;

/**
 * Creado por Nicolás en 13/04/2018.
 */
public class Sesion {
    private SharedPreferences preferences;
    private SharedPreferences.Editor editor;
    private Context contexto;

    @SuppressLint("CommitPrefEdits")
    public Sesion(Context context) {
        this.contexto = context;
        preferences = context.getSharedPreferences("MC", Context.MODE_PRIVATE);
        editor = preferences.edit();
    }

    public void setLogueado(boolean loggedin) {
        editor.putBoolean("loggedInmode", loggedin);
        editor.commit();
    }

    public boolean logueado() {
        return preferences.getBoolean("loggedInmode", false);
    }
}