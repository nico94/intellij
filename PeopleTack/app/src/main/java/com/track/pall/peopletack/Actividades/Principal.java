package com.track.pall.peopletack.Actividades;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.track.pall.peopletack.Modelos.Registro;
import com.track.pall.peopletack.Modelos.Usuario;
import com.track.pall.peopletack.R;
import com.track.pall.peopletack.Servicios.Servicio_GPS;

import java.util.*;

public class Principal extends AppCompatActivity implements GoogleApiClient.OnConnectionFailedListener {
    private static final String TAG = Principal.class.getSimpleName();
    private Button btn_ingresar;
    private Button btn_fin;
    private SignInButton btn_google;
    private static final int RC_SIGN_IN = 1;
    private Timer mTimer;
    private GoogleApiClient googleApiClient;
    private FirebaseAuth firebaseAuth;
    private FirebaseAuth.AuthStateListener authStateListener;
    private FirebaseDatabase database = FirebaseDatabase.getInstance();
    private static final int REQUEST_ID_MULTIPLE_PERMISSIONS = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_principal);
        btn_google = findViewById(R.id.btn_google);
        btn_ingresar = findViewById(R.id.btn_ingreso);
        btn_fin = findViewById(R.id.btn_fin);
        eventos();
        configurarGoogleAuth();
        verificarPermisos();
    }

    private boolean verificarPermisos() {
        if (android.os.Build.VERSION.SDK_INT >= 23) {
            int gps = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION);
            int gps2 = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION);
            List<String> listaPermisos = new ArrayList<>();
            if (gps != PackageManager.PERMISSION_GRANTED) {
                listaPermisos.add(android.Manifest.permission.ACCESS_FINE_LOCATION);
            }
            if (gps2 != PackageManager.PERMISSION_GRANTED) {
                listaPermisos.add(android.Manifest.permission.ACCESS_COARSE_LOCATION);
            }
            if (!listaPermisos.isEmpty()) {
                ActivityCompat.requestPermissions(this, listaPermisos.toArray(new String[listaPermisos.size()]), REQUEST_ID_MULTIPLE_PERMISSIONS);
                return false;
            }
        }
        return true;
    }

    private void eventos() {
        btn_google.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loginGoogle();
            }
        });
        btn_ingresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                iniciarServicio();
            }
        });
        btn_fin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pararServicio();
            }
        });
    }

    private void configurarGoogleAuth() {
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN).requestEmail().build();
        googleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();
        firebaseAuth = FirebaseAuth.getInstance();
        authStateListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                Log.d(TAG, "CAMBIO ESTADO: "+firebaseAuth.toString());
            }
        };
    }

    private void loginGoogle() {
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(googleApiClient);
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult resultado = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            manejarResultado(resultado);
        }
    }

    private void manejarResultado(GoogleSignInResult resultado) {
        if (resultado.isSuccess()) {
            GoogleSignInAccount acct = resultado.getSignInAccount();
            if (acct.getEmail() != null) {
                SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences(getString(R.string.FCM_PREF), Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putString("uid", acct.getId());
                editor.putString("nombre", acct.getDisplayName());
                editor.putString("correo", acct.getEmail());
                editor.apply();
                if (acct.getId() != null) {
                    DatabaseReference referenciaDB = database.getReference(acct.getId());
                    Usuario usuario = new Usuario();
                    usuario.setIdToken(acct.getIdToken());
                    usuario.setCorreo(acct.getEmail());
                    usuario.setNombre(acct.getDisplayName());
                    if (acct.getPhotoUrl() != null) {
                        usuario.setPhotoUrl(Objects.requireNonNull(acct.getPhotoUrl()).toString());
                    }
                    referenciaDB.setValue(usuario);
                }
            }
        }
    }

    private void iniciarServicio() {
        SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences(getString(R.string.FCM_PREF), Context.MODE_PRIVATE);
        String id = sharedPreferences.getString("uid", "");
        DatabaseReference referencia = database.getReference(id);
        Registro registro = new Registro();
        Date dat = new Date();
        String fecha = (String) android.text.format.DateFormat.format("dd-MM-yyyy HH:mm:ss", dat.getTime());
        registro.setInicio(fecha);
        referencia.child("registro").setValue(registro);
        if (mTimer != null) {
            mTimer.cancel();
        } else {
            mTimer = new Timer();
        }
        Intent servicioIntent = new Intent(this,Servicio_GPS.class);
        ContextCompat.startForegroundService(this,servicioIntent);
    }

    private void pararServicio() {
        SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences(getString(R.string.FCM_PREF), Context.MODE_PRIVATE);
        String id = sharedPreferences.getString("uid", "");
        DatabaseReference referencia = database.getReference(id);
        Registro registro = new Registro();
        Date dat = new Date();
        String fecha = (String) android.text.format.DateFormat.format("dd-MM-yyyy HH:mm:ss", dat.getTime());
        registro.setFin(fecha);
        referencia.child("registro").setValue(registro);
        getBaseContext().stopService(new Intent(getBaseContext(), Servicio_GPS.class));
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.d(TAG, "FALLO AL REGISTRAR USUARIO " + connectionResult.toString());
    }
}
