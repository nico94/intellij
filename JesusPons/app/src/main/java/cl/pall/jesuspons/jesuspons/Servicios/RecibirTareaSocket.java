package cl.pall.jesuspons.jesuspons.Servicios;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

import com.github.nkzawa.emitter.Emitter;
import com.github.nkzawa.socketio.client.IO;
import com.github.nkzawa.socketio.client.Socket;

import org.json.JSONObject;

import java.net.URISyntaxException;

import cl.pall.jesuspons.jesuspons.Actividades.HomeUsuario;
import cl.pall.jesuspons.jesuspons.R;

/**
 * Creado por Nicolás en 10/05/2018.
 */
public class RecibirTareaSocket extends Service{
    private static final String TAG = RecibirTareaSocket.class.getSimpleName();

    private Socket mSocket;
    {
        try {
            mSocket = IO.socket("http://186.10.19.170:8081");
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onCreate(){
        conectarSocket();
    }

    private void conectarSocket(){
        mSocket.connect();
        mSocket.on(Socket.EVENT_CONNECT, new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                //mSocket.emit("cedula","Hola");
                Log.d(TAG, "CONECTADO AL SOCKET");
                mSocket.on("notificacion", new Emitter.Listener() {
                    @Override
                    public void call(Object... args) {
                        String mensaje;
                        JSONObject obj = (JSONObject) args[0];
                        mensaje = obj.optString("mensaje");
                        mostrarNotificacion(mensaje);
                    }
                });
            }
        }).on(Socket.EVENT_DISCONNECT, new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                Log.d(TAG, "NO CONECTADO AL SOCKET");
            }
        });
    }

    private void mostrarNotificacion(String mensaje) {
            Intent intent = new Intent(this, HomeUsuario.class);
            Uri notificacion = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_ONE_SHOT);

            Bitmap icono = BitmapFactory.decodeResource(getResources(), R.drawable.tareas);
            Notification.Builder notificationBuilder = new Notification.Builder(this)
                    .setSmallIcon(R.drawable.tarea)
                    .setLargeIcon(icono)
                    .setContentTitle(mensaje)
                    .setContentText("Prueba")
                    .setSound(notificacion)
                    .setAutoCancel(true)
                    .setPriority(Notification.PRIORITY_MAX)
                    .addAction(R.drawable.verificacion, "Marcar como leída", null)
                    .setContentIntent(pendingIntent);

            notificationBuilder.setStyle(new Notification.BigTextStyle(notificationBuilder)
                    .bigText(mensaje)
                    .setSummaryText("Resumen notificacion")
            );

            NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            assert notificationManager != null;
            notificationManager.notify(0, notificationBuilder.build());
    }


    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flag, int idProcess){
        super.onStartCommand(intent,flag,idProcess);
        //conectarSocket();
        return START_NOT_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }
}
