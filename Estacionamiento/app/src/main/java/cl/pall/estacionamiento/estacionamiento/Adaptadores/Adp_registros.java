package cl.pall.estacionamiento.estacionamiento.Adaptadores;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import cl.pall.estacionamiento.estacionamiento.Modelos.Transaccion;
import cl.pall.estacionamiento.estacionamiento.PopUps.Detalle_vehiculo;
import cl.pall.estacionamiento.estacionamiento.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Adp_registros extends RecyclerView.Adapter<Adp_registros.ViewHolder> implements Filterable {
    private static final String TAG = Adp_registros.class.getSimpleName();
    private Context context;
    private List<Transaccion> transaccionList;
    private List<Transaccion> filtradas;
    private FiltroRegistros filtroRegistros;

    public Adp_registros(Context context, List<Transaccion> transaccionList) {
        super();
        this.context = context;
        this.filtradas = new ArrayList<>();
        this.transaccionList = transaccionList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.tarjeta_registro, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int position) {
        final Transaccion trans = transaccionList.get(position);
        viewHolder.tar_patente.setText(trans.getPatente());
        viewHolder.tar_fecha_ini.setText(trans.getFechaIngreso());
        viewHolder.tar_hora_ini.setText(trans.getHoraIngreso());
        viewHolder.tar_fecha_fin.setText(trans.getFechaSalida());
        viewHolder.tar_hora_fin.setText(trans.getHoraSalida());
        viewHolder.tar_tiempo.setText("");
        int color = 0;
        Drawable drawable;
        switch (trans.getEstado()) {
            case 0:
                viewHolder.lin_fecha_fin.setVisibility(View.GONE);
                color = ContextCompat.getColor(context, R.color.rojo);
                break;
            case 1:
                viewHolder.lin_fecha_fin.setVisibility(View.VISIBLE);
                color = ContextCompat.getColor(context, R.color.verde);
                break;
            default:
                break;
        }
        drawable = new ColorDrawable(color);
        viewHolder.estadoColor.setForeground(drawable);

        //Para calcular Tiempo
        String tiempo_transcurrido = "";
        Date ingreso = null;
        Date actual = null;
        Date salida = null;
        String fechaIngreso = trans.getFechaIngreso();
        String horaIngreso = trans.getHoraIngreso();
        Date dat = new Date();
        String fechaActual = (String) android.text.format.DateFormat.format("dd-MM-yyyy", dat.getTime());
        String horaActual = (String) android.text.format.DateFormat.format("HH:mm:ss", dat.getTime());
        long diferencia = 0;
        SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        try {
            ingreso = format.parse(fechaIngreso + " " + horaIngreso);
            actual = format.parse(fechaActual + " " + horaActual);
            salida = format.parse(trans.getFechaSalida() + " " + trans.getHoraSalida());
            if (trans.getEstado() == 0) {
                diferencia = actual.getTime() - ingreso.getTime();
            } else {
                diferencia = salida.getTime() - ingreso.getTime();
            }
            long segMili = 1000;
            long minMili = segMili * 60;
            long horaMili = minMili * 60;
            long diasMili = horaMili * 24;
            long diasTranscurridos = diferencia / diasMili;
            diferencia = diferencia % diasMili;
            long horasTranscurridas = diferencia / horaMili;
            diferencia = diferencia % horaMili;
            long minutosTranscurridos = diferencia / minMili;
            diferencia = diferencia % minMili;
            long segundosTranscurridos = diferencia / segMili;
            if (diasTranscurridos > 0) {
                tiempo_transcurrido = diasTranscurridos + " días ";
            }
            if (horasTranscurridas > 0) {
                tiempo_transcurrido = tiempo_transcurrido + horasTranscurridas + " horas ";
            }
            if (minutosTranscurridos > 0) {
                tiempo_transcurrido = tiempo_transcurrido + minutosTranscurridos + " minutos ";
            }
            tiempo_transcurrido = tiempo_transcurrido + segundosTranscurridos + " segundos";

        } catch (ParseException e) {
            e.printStackTrace();
        }
        viewHolder.tar_tiempo.setText(tiempo_transcurrido);
        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent abrirDetalle = new Intent(context, Detalle_vehiculo.class);
                abrirDetalle.putExtra("id", trans.getId());
                context.startActivity(abrirDetalle);
            }
        });
    }

    @Override
    public int getItemCount() {
        return this.transaccionList.size();
    }

    @Override
    public Filter getFilter() {
        if (filtroRegistros == null) {
            filtradas.clear();
            filtradas.addAll(this.transaccionList);
            filtroRegistros = new Adp_registros.FiltroRegistros(this, filtradas);
        }
        return filtroRegistros;
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tar_patente, tar_fecha_ini, tar_hora_ini, tar_fecha_fin, tar_hora_fin, tar_tiempo;
        LinearLayout lin_fecha_fin;
        private FrameLayout estadoColor;

        ViewHolder(@NonNull View itemView) {
            super(itemView);
            lin_fecha_fin = itemView.findViewById(R.id.lin_fecha_fin);
            tar_patente = itemView.findViewById(R.id.tar_patente);
            tar_fecha_ini = itemView.findViewById(R.id.tar_fecha_ini);
            tar_hora_ini = itemView.findViewById(R.id.tar_hora_ini);
            tar_fecha_fin = itemView.findViewById(R.id.tar_fecha_fin);
            tar_hora_fin = itemView.findViewById(R.id.tar_hora_fin);
            tar_tiempo = itemView.findViewById(R.id.tar_tiempo);
            estadoColor = itemView.findViewById(R.id.fr_estado);
        }
    }

    class FiltroRegistros extends Filter {
        private final Adp_registros adp_registros;
        private final List<Transaccion> originalList;
        private final List<Transaccion> filtradasList;

        FiltroRegistros(Adp_registros adp_registros, List<Transaccion> originalList) {
            this.adp_registros = adp_registros;
            this.originalList = originalList;
            this.filtradasList = new ArrayList<>();
        }

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            filtradasList.clear();
            FilterResults results = new FilterResults();
            if (constraint.length() == 0) {
                filtradasList.addAll(originalList);
            } else {
                final String patron = constraint.toString().toLowerCase().trim();
                for (Transaccion transaccion : originalList) {
                    if (transaccion.getPatente().toLowerCase().contains(patron)) {
                        filtradasList.add(transaccion);
                    }
                }
            }
            results.values = filtradasList;
            results.count = filtradasList.size();
            return results;
        }

        @SuppressWarnings("unchecked")
        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            adp_registros.transaccionList.clear();
            adp_registros.transaccionList.addAll((ArrayList<Transaccion>) results.values);
            adp_registros.notifyDataSetChanged();
        }
    }
}