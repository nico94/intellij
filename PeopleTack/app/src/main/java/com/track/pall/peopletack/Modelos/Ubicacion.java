package com.track.pall.peopletack.Modelos;

public class Ubicacion {
    private double latitud;
    private double longitud;
    private String fecha;

    public Ubicacion() {
    }

    public Ubicacion(double latitud, double longitud, String fecha) {
        this.latitud = latitud;
        this.longitud = longitud;
        this.fecha = fecha;
    }


    public double getLatitud() {
        return latitud;
    }

    public void setLatitud(double latitud) {
        this.latitud = latitud;
    }

    public double getLongitud() {
        return longitud;
    }

    public void setLongitud(double longitud) {
        this.longitud = longitud;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }
}
