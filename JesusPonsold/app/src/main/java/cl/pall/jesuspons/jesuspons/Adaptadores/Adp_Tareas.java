package cl.pall.jesuspons.jesuspons.Adaptadores;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.FrameLayout;
import android.widget.TextView;
import cl.pall.jesuspons.jesuspons.Modelos.Tarea;
import cl.pall.jesuspons.jesuspons.PopUps.Detalle_tarea;
import cl.pall.jesuspons.jesuspons.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Creado por Nicolás en 16/04/2018.
 */
public class Adp_Tareas extends RecyclerView.Adapter<Adp_Tareas.ViewHolder> implements Filterable {
    //private static final String TAG = Adp_Tareas.class.getSimpleName();
    private Context contexto;
    private List<Tarea> listTareas;
    private List<Tarea> filtradas;
    private FiltroTareas filtroTareas;

    public Adp_Tareas(List<Tarea> listaTareas, Context context) {
        super();
        this.listTareas = listaTareas;
        this.filtradas = new ArrayList<>();
        this.contexto = context;
    }

    @NonNull
    @Override
    public Adp_Tareas.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.tarjeta_tareas, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull Adp_Tareas.ViewHolder holder, int position) {
        final Tarea tareas = listTareas.get(position);
        final int id = tareas.getId();
        int estado = tareas.getEstado();
        String detalle = tareas.getDetalle();
        String responsable = tareas.getResponsable();
        String fecha = tareas.getFecha_crea();
        String fecha_exp = tareas.getFecha_exp();
        holder.detalle.setText(detalle);
        holder.responsable.setText(responsable);
        holder.tar_auto.setText(tareas.getMarca()+" "+tareas.getModelo());
        holder.fecha.setText(fecha);
        holder.tar_cierre_negocio.setText("Cierre negocio " + String.valueOf(tareas.getId_cierre_negocio()));
        holder.fecha_exp.setText(fecha_exp);
        int color = 0;
        Drawable drawable;
        switch (estado) {
            case 1:
                color = ContextCompat.getColor(contexto, R.color.rojo);
                break;
            case 2:
                color = ContextCompat.getColor(contexto, R.color.amarillo);
                break;
            case 3:
                color = ContextCompat.getColor(contexto, R.color.verde);
                break;
            default:
                break;
        }
        drawable = new ColorDrawable(color);
        holder.estadoColor.setForeground(drawable);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (id > 0) {
                    Intent intent = new Intent(contexto, Detalle_tarea.class);
                    intent.putExtra("id_tarea", id);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    contexto.startActivity(intent);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return listTareas.size();
    }

    @Override
    public Filter getFilter() {
        if (filtroTareas == null) {
            filtradas.clear();
            filtradas.addAll(this.listTareas);
            filtroTareas = new Adp_Tareas.FiltroTareas(this, filtradas);
        }
        return filtroTareas;
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        private TextView detalle, responsable, fecha, fecha_exp, tar_cierre_negocio, tar_auto;
        private FrameLayout estadoColor;

        private ViewHolder(View itemView) {
            super(itemView);
            detalle = itemView.findViewById(R.id.tar_detalle);
            responsable = itemView.findViewById(R.id.tar_responsable);
            tar_auto = itemView.findViewById(R.id.tar_auto);
            fecha = itemView.findViewById(R.id.tar_fecha);
            fecha_exp = itemView.findViewById(R.id.tar_fecha_exp);
            tar_cierre_negocio = itemView.findViewById(R.id.tar_cierre_negocio);
            estadoColor = itemView.findViewById(R.id.fr_estado);
        }
    }

    class FiltroTareas extends Filter {
        private final Adp_Tareas adp_tareas;
        private final List<Tarea> originalList;
        private final List<Tarea> filtradaList;

        FiltroTareas(Adp_Tareas adp_tareas, List<Tarea> originalList) {
            this.adp_tareas = adp_tareas;
            this.originalList = originalList;
            this.filtradaList = new ArrayList<>();
        }

        @Override
        protected FilterResults performFiltering(CharSequence charSequence) {
            filtradaList.clear();
            FilterResults resultados = new FilterResults();
            if (charSequence.length() == 0) {
                filtradaList.addAll(originalList);
            } else {
                final String patron = charSequence.toString().toLowerCase().trim();
                for (Tarea tareas : originalList) {
                    if (tareas.getDetalle().toLowerCase().contains(patron) || tareas.getResponsable().toLowerCase().contains(patron)) {
                        filtradaList.add(tareas);
                    }
                }
            }
            resultados.values = filtradaList;
            resultados.count = filtradaList.size();
            return resultados;
        }

        @SuppressWarnings("unchecked")
        @Override
        protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
            adp_tareas.listTareas.clear();
            adp_tareas.listTareas.addAll((ArrayList<Tarea>) filterResults.values);
            adp_tareas.notifyDataSetChanged();
        }
    }
}