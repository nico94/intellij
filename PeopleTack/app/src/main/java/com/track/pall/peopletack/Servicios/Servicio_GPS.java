package com.track.pall.peopletack.Servicios;

import android.Manifest;
import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.widget.Toast;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.*;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.*;
import com.track.pall.peopletack.Actividades.Principal;
import com.track.pall.peopletack.Modelos.Ubicacion;
import com.track.pall.peopletack.R;

import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

import static com.track.pall.peopletack.Aplicacion.CHANNEL_ID;

public class Servicio_GPS extends Service implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {
    private final static String TAG = Servicio_GPS.class.getSimpleName();
    private FusedLocationProviderClient mFusedLocationClient;
    private LocationRequest locationRequest;
    private LocationCallback mLocationCallback;
    //private static final long INTERVALO = 900000; //15 minutos
//    private static final long INTERVALO = 100 * 1000; //60 segundos
    private static final long INTERVALO = 1000; //1 segundos
    private Handler handler = new Handler();
    private Timer timer;
    private double latitud;
    private double longitud;
    private FirebaseDatabase database = FirebaseDatabase.getInstance();

    @Override
    public void onCreate() {
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        if (timer != null) {
            timer.cancel();
        } else {
            timer = new Timer();
        }
        obtenerTiempo();
        iniciarServicio();
        super.onCreate();
    }

    private void obtenerTiempo() {
        DatabaseReference referencia = database.getReference("/");
        referencia.child("tiempo").child("tiempo").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(dataSnapshot.exists()){

                }

                Log.d(TAG, "onDataChange: "+dataSnapshot.getValue());
                Toast.makeText(Servicio_GPS.this, "RES "+dataSnapshot, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    @Override
    public void onDestroy() {
        pararServicio();
        super.onDestroy();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    private void iniciarServicio() {
        Log.d(TAG, "INICIADO SERVICIO GPS");
        ultimaUbicacion();
        crearLocationRecuest();
        crearCallback();
        iniciarActualizaciones();
    }

    class PararServicio extends TimerTask {
        @Override
        public void run() {
            handler.post(new Runnable() {
                @Override
                public void run() {
                    pararServicio();
                }
            });
        }
    }

    private void pararServicio() {
        Log.d(TAG, "PARAR SERVICIO GPS");
        mFusedLocationClient.removeLocationUpdates(mLocationCallback);
    }

    @Override
    public int onStartCommand(Intent intent, int flag, int idProcess) {
        Intent notificacionIntent = new Intent(this, Principal.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, notificacionIntent, 0);
        Notification notification = new NotificationCompat.Builder(this, CHANNEL_ID)
                .setContentTitle("Estoy en terreno")
                .setContentText("Verificado")
                .setSmallIcon(R.drawable.estrella)
                .setContentIntent(pendingIntent)
                .build();
        startForeground(1, notification);
        return START_NOT_STICKY;
    }

    private void iniciarActualizaciones() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        mFusedLocationClient.requestLocationUpdates(locationRequest, mLocationCallback, null);
    }

    private void crearCallback() {
        mLocationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences(getString(R.string.FCM_PREF), Context.MODE_PRIVATE);
                String id = sharedPreferences.getString("uid", "");
                DatabaseReference referenciaDB = database.getReference(id);

                for (Location location : locationResult.getLocations()) {
                    Date dat = new Date();
                    String fecha = (String) android.text.format.DateFormat.format("dd-MM-yyyy", dat.getTime());
                    String hora = (String) android.text.format.DateFormat.format("HH:mm:ss", dat.getTime());
                    Ubicacion ubicacion = new Ubicacion();
                    ubicacion.setLatitud(location.getLatitude());
                    ubicacion.setLongitud(location.getLongitude());
                    ubicacion.setFecha(fecha + " " + hora);

                    DatabaseReference newRef = referenciaDB.child("ubicaciones");
                    DatabaseReference actual = referenciaDB.child("actual");
                    actual.setValue(ubicacion);
                    newRef.child(fecha).child(hora).setValue(ubicacion);
                }
                super.onLocationResult(locationResult);
            }
        };
    }

    private void crearLocationRecuest() {
        locationRequest = LocationRequest.create();
        locationRequest.setInterval(INTERVALO);
        locationRequest.setFastestInterval(1000);
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }

    private void ultimaUbicacion() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        mFusedLocationClient.getLastLocation()
                .addOnSuccessListener(new OnSuccessListener<Location>() {
                    @Override
                    public void onSuccess(Location location) {
                        if (location != null) {
                            latitud = location.getLatitude();
                            longitud = location.getLongitude();
                            Log.d(TAG, "ULTIMA UBICACION: LAT: " + String.valueOf(latitud) + " LON: " + String.valueOf(longitud));
                        }
                    }
                });
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.d(TAG, "FALLO CONEXION: " + connectionResult.toString());
    }
}