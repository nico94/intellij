package cl.pall.estacionamiento.estacionamiento.Fragments;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import cl.pall.estacionamiento.estacionamiento.Actividades.Inicio;
import cl.pall.estacionamiento.estacionamiento.R;

import java.util.Objects;

public class Configuracion extends Fragment {
    private static final String TAG = Configuracion.class.getSimpleName();
    private View vistaRoot;
    private Switch swith_modo_telefono, swt_imprime_ingreso, swt_imprime_despacho, swt_menu_ingreso, swt_menu_despacho, swt_imprime_qr;
    private String ip_server, imprime_despacho, imprime_ingreso, modo_telefono, menu_ingreso, menu_despacho, impime_qr;
    private EditText edt_ip;
    private Button btn_guardar_ip;

    public Configuracion() {
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        vistaRoot = inflater.inflate(R.layout.fragment_configuracion, container, false);
        edt_ip = vistaRoot.findViewById(R.id.edt_ip);
        btn_guardar_ip = vistaRoot.findViewById(R.id.btn_guardar_ip);
        swith_modo_telefono = vistaRoot.findViewById(R.id.swith_modo_telefono);
        swt_menu_ingreso = vistaRoot.findViewById(R.id.swt_solo_ingreso);
        swt_menu_despacho = vistaRoot.findViewById(R.id.swt_solo_despacho);
        swt_imprime_ingreso = vistaRoot.findViewById(R.id.swt_imprime_ingreso);
        swt_imprime_despacho = vistaRoot.findViewById(R.id.swt_imprime_despacho);
        swt_imprime_qr = vistaRoot.findViewById(R.id.swt_imprime_qr);
        cargarConfiguraciones();
        eventos();
        return vistaRoot;
    }

    private void eventos() {
        final SharedPreferences preferencias = Objects.requireNonNull(getActivity()).getSharedPreferences(getString(R.string.PREF), Context.MODE_PRIVATE);
        btn_guardar_ip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String ip = edt_ip.getText().toString().trim();
                if (ip.length() > 0) {
                    SharedPreferences.Editor editor = preferencias.edit();
                    editor.putString("ip_server", ip);
                    editor.apply();
                    Toast.makeText(getContext(), "IP guardada correctamente", Toast.LENGTH_SHORT).show();
                    cargarConfiguraciones();
                }
            }
        });
        swith_modo_telefono.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                SharedPreferences.Editor editor = preferencias.edit();
                editor.putString("modo_telefono", String.valueOf(isChecked));
                editor.apply();
                cargarConfiguraciones();
            }
        });
        swt_menu_ingreso.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                SharedPreferences.Editor editor = preferencias.edit();
                editor.putString("menu_ingreso", String.valueOf(isChecked));
                editor.apply();
                cargarConfiguraciones();
            }
        });
        swt_menu_despacho.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                SharedPreferences.Editor editor = preferencias.edit();
                editor.putString("menu_despacho", String.valueOf(isChecked));
                editor.apply();
                cargarConfiguraciones();
            }
        });
        swt_imprime_qr.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                SharedPreferences.Editor editor = preferencias.edit();
                editor.putString("imp_qr", String.valueOf(isChecked));
                editor.apply();
                cargarConfiguraciones();
            }
        });
        swt_imprime_ingreso.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                SharedPreferences.Editor editor = preferencias.edit();
                editor.putString("imp_ingreso", String.valueOf(isChecked));
                editor.apply();
                cargarConfiguraciones();
            }
        });
        swt_imprime_despacho.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                SharedPreferences.Editor editor = preferencias.edit();
                editor.putString("imp_despacho", String.valueOf(isChecked));
                editor.apply();
                cargarConfiguraciones();
            }
        });
    }

    private void cargarConfiguraciones() {
        SharedPreferences pref = Objects.requireNonNull(getActivity()).getSharedPreferences(getString(R.string.PREF), Context.MODE_PRIVATE);
        ip_server = pref.getString("ip_server", "");
        modo_telefono = pref.getString("modo_telefono", "");
        menu_ingreso = pref.getString("menu_ingreso", "");
        menu_despacho = pref.getString("menu_despacho", "");
        imprime_ingreso = pref.getString("imp_ingreso", "");
        imprime_despacho = pref.getString("imp_despacho", "");
        impime_qr = pref.getString("imp_qr", "");
        edt_ip.setText(ip_server);
        swith_modo_telefono.setChecked(Boolean.parseBoolean(modo_telefono));
        swt_menu_ingreso.setChecked(Boolean.parseBoolean(menu_ingreso));
        swt_menu_despacho.setChecked(Boolean.parseBoolean(menu_despacho));
        swt_imprime_ingreso.setChecked(Boolean.parseBoolean(imprime_ingreso));
        swt_imprime_despacho.setChecked(Boolean.parseBoolean(imprime_despacho));
        swt_imprime_qr.setChecked(Boolean.parseBoolean(impime_qr));
    }
}