package cl.pall.jesuspons.jesuspons.BD;

/**
 * Creado por Nicolás en 13/04/2018.
 */
public class Sql {
    //CREACION DE TABLAS
    static String TABLA_USUARIO = "Usuario";
    static String TABLA_TAREAS = "Tarea";

    //COLUMNAS USUARIO
    static String ID = "id";
    static String COL_CODIGO = "codigo";
    static String COL_COD_CARGO = "cod_cargo";
    static String COL_USUARIO = "usuario";
    static String COL_PASSWORD = "password";
    static String COL_RUT = "rut";
    static String COL_NOMBRES = "nombres";
    static String COL_AP_PATERNO = "ap_paterno";
    static String COL_AP_MATERNO = "ap_materno";
    static String COL_FECHA_NACIMIENTO = "fecha_nacimiento";
    static String COL_EMAIL = "email";
    static String COL_TELEFONO = "telefono";
    static String COL_DIRECCION = "direccion";
    static String COL_COD_COMUNA = "cod_comuna";

    //COMUMNAS TAREAS
    static String COL_ID_ACCION = "id_accion";
    static String COL_ID_CIERRE = "id_cierre_negocio";
    static String COL_COD_FICHA_PERSONAL = "cod_ficha_personal";
    static String COL_RESPONSABLE = "responsable";
    static String COL_ESTADO = "estado";
    static String COL_LECTURA = "lectura";
    static String COL_DETALLE = "detalle";
    static String COL_MARCA = "marca";
    static String COL_MODELO = "modelo";
    static String COL_VIN = "vin";
    static String COL_FECHA_CREA = "fecha_crea";
    static String COL_HORA_CREA = "hora_crea";
    static String COL_FECHA_EXP = "fecha_exp";
    static String COL_HORA_EXP = "hora_exp";
    static String COL_FECHA_INI = "fecha_ini";
    static String COL_HORA_INI = "hora_ini";
    static String COL_FECHA_FIN = "fecha_fin";
    static String COL_HORA_FIN = "hora_fin";

    //CREATE TABLES
    static String CREAR_TABLA_USUARIO = "CREATE TABLE " + TABLA_USUARIO + " ("
            + ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + COL_CODIGO + " TEXT,"
            + COL_COD_CARGO + " TEXT,"
            + COL_USUARIO + " TEXT,"
            + COL_PASSWORD + " TEXT,"
            + COL_RUT + " TEXT,"
            + COL_NOMBRES + " TEXT,"
            + COL_AP_PATERNO + " TEXT,"
            + COL_AP_MATERNO + " TEXT,"
            + COL_FECHA_NACIMIENTO + " TEXT,"
            + COL_EMAIL + " TEXT,"
            + COL_TELEFONO + " TEXT,"
            + COL_DIRECCION + " TEXT,"
            + COL_COD_COMUNA + " TEXT)";

    static String CREAR_TABLA_TAREAS = "CREATE TABLE Tarea ("
            + ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + COL_ID_ACCION + " INTEGER,"
            + COL_ID_CIERRE + " INTEGER,"
            + COL_COD_FICHA_PERSONAL + " INTEGER,"
            + COL_RESPONSABLE + " TEXT,"
            + COL_ESTADO + " INTEGER,"
            + COL_LECTURA + " INTEGER,"
            + COL_DETALLE + " TEXT,"
            + COL_MARCA + " TEXT,"
            + COL_MODELO + " TEXT,"
            + COL_VIN + " TEXT,"
            + COL_FECHA_CREA + " TEXT,"
            + COL_HORA_CREA + " TEXT,"
            + COL_FECHA_EXP + " TEXT,"
            + COL_HORA_EXP + " TEXT,"
            + COL_FECHA_INI + " TEXT,"
            + COL_HORA_INI + " TEXT,"
            + COL_FECHA_FIN + " TEXT,"
            + COL_HORA_FIN + " TEXT)";
}