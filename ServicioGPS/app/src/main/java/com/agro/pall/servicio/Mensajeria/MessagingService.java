package com.agro.pall.servicio.Mensajeria;

import android.content.Intent;
import android.os.Handler;
import android.util.Log;
import com.agro.pall.servicio.Servicios.GPS_service;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

public class MessagingService extends FirebaseMessagingService {
    private static final String TAG = MessagingService.class.getSimpleName();

    private static final long INTERVALO = 10 * 1000; //10 segundos
    private Timer mTimer;
    private Handler mHandler;

    @Override
    public void onNewToken(String s){
        super.onNewToken(s);
        Log.i(TAG, "TOKEN : "+s);
    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);
        Map<String, String> data = remoteMessage.getData();
        if (remoteMessage.getData().size() > 0) {
            Log.d(TAG, "Mensaje: " + remoteMessage.getData().toString());
            String estado = data.get("estado");
            if (estado.equals("true")) {
                iniciarServicio();
            }
        }
    }

    private void iniciarServicio() {
        Log.d(TAG, "INICIA SERVICIO");
        if (mTimer != null) {
            mTimer.cancel();
        } else {
            mTimer = new Timer();
        }
        getBaseContext().startService(new Intent(getBaseContext(), GPS_service.class));
        mTimer.schedule(new PararServicio(), INTERVALO);
    }

    private void pararServicio1(){
        Log.d(TAG, "PARAR SERVICIO");
        getBaseContext().stopService(new Intent(getBaseContext(), GPS_service.class));
    }

    class PararServicio extends TimerTask {
        @Override
        public void run() {
            pararServicio1();
        }
    }
}