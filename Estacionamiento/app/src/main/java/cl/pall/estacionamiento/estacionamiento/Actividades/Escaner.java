package cl.pall.estacionamiento.estacionamiento.Actividades;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.util.SparseArray;
import android.widget.Toast;
import cl.pall.estacionamiento.estacionamiento.R;
import com.google.android.gms.vision.barcode.Barcode;
import info.androidhive.barcode.BarcodeReader;

import java.util.List;

public class Escaner extends AppCompatActivity implements BarcodeReader.BarcodeReaderListener {
    private static final String TAG = Escaner.class.getSimpleName();
    private BarcodeReader barcodeReader;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG, "INICIA ESCANER");
        setContentView(R.layout.activity_escaner);
        barcodeReader = (BarcodeReader) getSupportFragmentManager().findFragmentById(R.id.barcode_fragment);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    public void onScanned(final Barcode barcode) {
        barcodeReader.playBeep();
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (barcode.displayValue.length() == 13) {
                    Intent data = new Intent();
                    String escaneo = barcode.displayValue;
                    data.setData(Uri.parse(escaneo));
                    setResult(RESULT_OK, data);
                    finish();
                } else {
                    Toast.makeText(Escaner.this, "Largo no corresponde (" + barcode.displayValue.length() + ")", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    public void onScannedMultiple(List<Barcode> barcodes) {

    }

    @Override
    public void onBitmapScanned(SparseArray<Barcode> sparseArray) {

    }

    @Override
    public void onScanError(final String errorMessage) {
        Log.d(TAG, "onScanError: " + errorMessage);
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(getApplication(), "Error " + errorMessage, Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onCameraPermissionDenied() {
        Log.d(TAG, "onCameraPermissionDenied");
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(getApplication(), "Sin permisos para escaner", Toast.LENGTH_SHORT).show();
            }
        });
    }
}