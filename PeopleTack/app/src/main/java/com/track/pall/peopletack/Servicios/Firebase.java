package com.track.pall.peopletack.Servicios;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.track.pall.peopletack.R;

import java.util.Map;

public class Firebase extends FirebaseMessagingService {
    private static final String TAG = Firebase.class.getSimpleName();

    @Override
    public void onNewToken(String token) {
        Log.d(TAG, "onNewToken: " + token);
        SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences(getString(R.string.FCM_PREF), Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(getString(R.string.FCM_TOKEN), token);
        editor.apply();
    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);
        Map<String, String> data = remoteMessage.getData();
        if (remoteMessage.getData().size() > 0) {
            Log.d(TAG, "Mensaje: " + remoteMessage.getData().toString());
        }
    }
}
