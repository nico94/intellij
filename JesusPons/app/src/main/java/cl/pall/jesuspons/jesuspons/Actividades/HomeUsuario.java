package cl.pall.jesuspons.jesuspons.Actividades;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Hashtable;
import java.util.Map;

import cl.pall.jesuspons.jesuspons.Adaptadores.Adp_ViewPagerHome;
import cl.pall.jesuspons.jesuspons.BD.DbHelper;
import cl.pall.jesuspons.jesuspons.BD.Sesion;
import cl.pall.jesuspons.jesuspons.Modelos.Usuario;
import cl.pall.jesuspons.jesuspons.R;
import cl.pall.jesuspons.jesuspons.Servicios.RecibirTareaSocket;
import cl.pall.jesuspons.jesuspons.Utils.Constant;
import cl.pall.jesuspons.jesuspons.WebServices.VolleySingleton;

public class HomeUsuario extends AppCompatActivity {
    private static final String TAG = HomeUsuario.class.getSimpleName();
    private Adp_ViewPagerHome adp_viewPagerHome;
    private ViewPager viewPager;

    private Sesion sesion;
    private DbHelper db;

    private VolleySingleton volleySingleton;
    private RequestQueue requestQueue;
    private static String token;
    private String nombreDispositivo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        //Toolbar
        Toolbar toolbar = findViewById(R.id.ToolbarPrincipal);
        db = new DbHelper(HomeUsuario.this);
        //TabLayout
        TabLayout tabLayout = findViewById(R.id.TabLayoutPrincipal);
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
        tabLayout.setTabMode(TabLayout.MODE_FIXED);
        tabLayout.addTab(tabLayout.newTab());
        tabLayout.addTab(tabLayout.newTab());
        tabLayout.addTab(tabLayout.newTab());

        viewPager = findViewById(R.id.ViewPagerPrincipal);
        adp_viewPagerHome = new Adp_ViewPagerHome(getSupportFragmentManager(), tabLayout.getTabCount());
        viewPager.setAdapter(adp_viewPagerHome);
        tabLayout.setupWithViewPager(viewPager);
        setSupportActionBar(toolbar);
        //getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Tarea");
        Usuario usu = db.obtenerUsuario();
        getSupportActionBar().setSubtitle(usu.getNombres());
        //Incializar Volley
        volleySingleton = VolleySingleton.getInstance(getApplicationContext().getApplicationContext());
        requestQueue = volleySingleton.getmRequestQueue();

        //Verificar la sesion
        sesion = new Sesion(HomeUsuario.this);
        if (!sesion.logueado()) {
            finish();
        }
        eventos();
    }

    private void eventos() {
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });
    }

    private void enviarToken() {
        SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences(getString(R.string.FCM_PREF), Context.MODE_PRIVATE);
        token = sharedPreferences.getString(getString(R.string.FCM_TOKEN), "");
        nombreDispositivo = Build.MODEL;
        JSONObject dataEnvio = new JSONObject();
        try {
            JSONObject padre = new JSONObject();
            padre.put("IdUsuario", "00111");
            padre.put("Usuario", "Nicolas");
            padre.put("Dispositivo", nombreDispositivo);
            padre.put("Token_str", token);
            dataEnvio.put("token", padre);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest peticion = new JsonObjectRequest(Request.Method.POST, Constant.URL_REGISTRO_TOKEN, dataEnvio, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.d(TAG, "RESPUESTA WS: " + response.toString());

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(TAG, "ERROR VOLLEY " + error.getMessage());
                if (error instanceof NetworkError) {
                    Log.e(TAG, "Error Network " + error.networkResponse.data.toString());
                } else if (error instanceof ServerError) {
                    Log.e(TAG, "Server Error: " + error.networkResponse.statusCode);
                } else if (error instanceof AuthFailureError) {
                    Log.e(TAG, "AuthFailureError: " + error.getMessage());
                } else if (error instanceof ParseError) {
                    Log.e(TAG, "ParseError: " + error.getMessage());
                } else if (error instanceof TimeoutError) {
                    Log.e(TAG, "TimeoutError " + error.getMessage());
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> cabeceras = new Hashtable<>();
                cabeceras.put("Content-Type", "application/json; charset=utf-8");
                return cabeceras;
            }
        };
        requestQueue = volleySingleton.getmRequestQueue();
        requestQueue.add(peticion);
    }
}