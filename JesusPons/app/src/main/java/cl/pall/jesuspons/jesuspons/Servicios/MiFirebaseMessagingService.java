package cl.pall.jesuspons.jesuspons.Servicios;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.content.LocalBroadcastManager;
import android.text.TextUtils;
import android.util.Log;

import cl.pall.jesuspons.jesuspons.Modelos.Tarea;
import cl.pall.jesuspons.jesuspons.Utils.Constant;
import cl.pall.jesuspons.jesuspons.WebServices.VolleySingleton;
import com.android.volley.*;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.firebase.messaging.RemoteMessage;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import cl.pall.jesuspons.jesuspons.Actividades.Inicio;
import cl.pall.jesuspons.jesuspons.BD.DbHelper;
import cl.pall.jesuspons.jesuspons.BD.Sesion;
import cl.pall.jesuspons.jesuspons.R;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Creado por Nicolás en 12/04/2018.
 */
public class MiFirebaseMessagingService extends com.google.firebase.messaging.FirebaseMessagingService {
    private static final String TAG = "Firebase";
    private DbHelper db = new DbHelper(this);
    private LocalBroadcastManager broadcaster;
    public static final String INFO_UPDATE_FILTER = "info_update_filter";
    private Tarea tarea;
    private Sesion sesion;

    @Override
    public void onNewToken(String token) {
        Log.d(TAG, "Token recibido en messaging: " + token);
        SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences(getString(R.string.FCM_PREF), Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(getString(R.string.FCM_TOKEN), token);
        editor.apply();
    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);
        Map<String, String> data = remoteMessage.getData();
        sesion = new Sesion(getApplicationContext());
        if (sesion.logueado()) {
            //Estilo mensaje de datos
            if (remoteMessage.getData().size() > 0) {
                tarea = new Tarea();
                tarea.setId_accion(Integer.parseInt(data.get("id_accion")));
                tarea.setId_cierre_negocio(Integer.parseInt(data.get("id_cierre_negocio")));
                tarea.setCod_ficha_personal(Integer.parseInt(data.get("cod_ficha_personal")));
                tarea.setResponsable(data.get("responsable"));
                tarea.setEstado(Integer.parseInt(data.get("estado")));
                tarea.setLectura(0);
                tarea.setDetalle(data.get("detalle"));
                tarea.setFecha_crea(data.get("fecha_crea"));
                tarea.setHora_crea(data.get("hora_crea"));
                tarea.setFecha_exp(data.get("fecha_exp"));
                tarea.setHora_exp(data.get("hora_exp"));
                tarea.setFecha_ini(data.get("fecha_ini"));
                tarea.setHora_ini(data.get("hora_ini"));
                tarea.setFecha_fin(data.get("fecha_fin"));
                tarea.setHora_fin(data.get("hora_fin"));
                if (tarea.getCod_ficha_personal() > 0 && tarea.getId_cierre_negocio() > 0 && tarea.getEstado() == 1 && !TextUtils.isEmpty(tarea.getDetalle())) {
                    mostrarNotificacion(tarea);
                }
//                if (tarea.getEstado() == 2 || tarea.getEstado() == 3) {
//                    if (db.verificaExisteTarea(tarea)) {
//                        db.modificarTarea(tarea);
//                    }
//                }
                actualizarInterfaz();
            }
            //Estilo notificacion
            if (remoteMessage.getNotification() != null) {
                mostrarNotificacion(tarea);
            }
        } else {
            Log.d(TAG, "NO HABILITADO PARA RECIBIR NOTIFICACIONES");
        }
    }

    private void actualizarInterfaz() {
        broadcaster = LocalBroadcastManager.getInstance(getBaseContext());
        Intent intent = new Intent(INFO_UPDATE_FILTER);
        intent.putExtra(INFO_UPDATE_FILTER, "");
        broadcaster.sendBroadcast(intent);
    }

    private void mostrarNotificacion(Tarea tarea) {
        if (tarea != null) {
            Intent intent = new Intent(this, Inicio.class);
            intent.putExtra("tarea", "new_tarea");
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            Uri notificacion = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_ONE_SHOT);

            Bitmap icono = BitmapFactory.decodeResource(getResources(), R.drawable.tareas);
            Notification.Builder notificationBuilder = new Notification.Builder(this)
                    .setSmallIcon(R.drawable.tarea)
                    .setLargeIcon(icono)
                    .setContentTitle("Nueva tarea asiganada")
                    .setContentText(tarea.getDetalle())
                    .setSound(notificacion)
                    .setAutoCancel(true)
                    .setOngoing(true)
//                    .setPriority(Notification.PRIORITY_MAX)
//                    .addAction(R.drawable.verificacion, "Marcar como leída", null)
                    .setContentIntent(pendingIntent);

            notificationBuilder.setStyle(new Notification.BigTextStyle(notificationBuilder)
                    .bigText(tarea.getDetalle())
                    .setSummaryText(tarea.getResponsable())
            );

            NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            assert notificationManager != null;
            notificationManager.notify(0, notificationBuilder.build());
        }
    }
}