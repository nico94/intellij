package com.track.pall.peopletack;

import android.app.Application;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.os.Build;

public class Aplicacion extends Application {
    public static final String CHANNEL_ID = "servicio GPS";


    @Override
    public void onCreate() {
        super.onCreate();
        crearCanalNotificacion();
    }

    private void crearCanalNotificacion() {
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O){
            NotificationChannel canalServicio = new NotificationChannel(CHANNEL_ID,"Servicio GPS", NotificationManager.IMPORTANCE_DEFAULT);
            NotificationManager manager = getSystemService(NotificationManager.class);
            assert manager != null;
            manager.createNotificationChannel(canalServicio);
        }
    }
}
