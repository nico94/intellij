package cl.pall.jesuspons.jesuspons.Fragments;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.view.inputmethod.InputMethodManager;
import android.widget.*;

import java.util.ArrayList;
import java.util.List;

import cl.pall.jesuspons.jesuspons.Actividades.Inicio;
import cl.pall.jesuspons.jesuspons.Adaptadores.Adp_Tareas;
import cl.pall.jesuspons.jesuspons.BD.DbHelper;
import cl.pall.jesuspons.jesuspons.Modelos.Tarea;
import cl.pall.jesuspons.jesuspons.Modelos.Usuario;
import cl.pall.jesuspons.jesuspons.R;
import cl.pall.jesuspons.jesuspons.Servicios.MiFirebaseMessagingService;

/**
 * A simple {@link Fragment} subclass.
 */
public class Frg_tareas extends Fragment implements SearchView.OnQueryTextListener, MenuItem.OnActionExpandListener {
    private static final String TAG = Frg_tareas.class.getSimpleName();
    private View vistaRoot;
    private List<Tarea> listaTareas = new ArrayList<>();
    private Adp_Tareas adp_tareas = new Adp_Tareas(listaTareas, getContext());
    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;
    private SwipeRefreshLayout refreshLayout;
    private DbHelper db;
    private LinearLayout lin_vacio;
    private Context mContext;
    private RadioGroup rd_group;
    private RadioButton rb_pendientes;
    private int seleccion = 1;

    public Frg_tareas() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = getActivity();
        setHasOptionsMenu(true);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        vistaRoot = inflater.inflate(R.layout.fragment_pagina1, container, false);
        recyclerView = vistaRoot.findViewById(R.id.pag1_reciclador);
        refreshLayout = vistaRoot.findViewById(R.id.pag1_swipe);
        lin_vacio = vistaRoot.findViewById(R.id.lin_vacio);
        rd_group = vistaRoot.findViewById(R.id.radioGroup);
        rb_pendientes = vistaRoot.findViewById(R.id.rb_pendientes);
        rb_pendientes.setChecked(true);
        listaTareas = new ArrayList<>();
        db = new DbHelper(getContext());
        recyclerView.setHasFixedSize(true);
        recyclerView.setItemViewCacheSize(20);
        layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setDrawingCacheEnabled(true);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adp_tareas);
        refreshLayout.setVisibility(View.GONE);
        lin_vacio.setVisibility(View.VISIBLE);
        eventos();
        cargaTareas(1);
        LocalBroadcastManager.getInstance(getContext()).registerReceiver(actualizar, new IntentFilter(MiFirebaseMessagingService.INFO_UPDATE_FILTER));
        return vistaRoot;
    }

    public void onDestroy() {
        super.onDestroy();
        LocalBroadcastManager.getInstance(getContext()).unregisterReceiver(actualizar);
    }

    private void eventos() {
        refreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                listaTareas = new ArrayList<>();
                cargaTareas(seleccion);
                refreshLayout.setRefreshing(false);
            }
        });
        refreshLayout.setColorSchemeResources(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);
        rd_group.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                int rad;
                seleccion = 1;
                RadioButton rb = vistaRoot.findViewById(i);
                rad = rd_group.indexOfChild(rb);
                if (rad == 0) {
                    seleccion = 1;
                } else if (rad == 1) {
                    seleccion = 2;
                } else if (rad == 2) {
                    seleccion = 3;
                }
                cargaTareas(seleccion);
            }
        });
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        inflater.inflate(R.menu.search_menu, menu);
        MenuItem searchItem = menu.findItem(R.id.action_search);
        SearchView searchView = (SearchView) searchItem.getActionView();
        searchView.setOnQueryTextListener(this);
        searchView.setQueryHint("Buscar tareas...");
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        adp_tareas.getFilter().filter(query);
        View view = getActivity().getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            assert imm != null;
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
        return true;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        adp_tareas.getFilter().filter(newText);
        iniciarAnimacion(recyclerView);
        return false;
    }

    @Override
    public boolean onMenuItemActionExpand(MenuItem item) {
        return true;
    }

    @Override
    public boolean onMenuItemActionCollapse(MenuItem item) {
        return true;
    }

    private void cargaTareas(final int estado) {
        final Usuario usu = db.obtenerUsuario();
        Inicio inicio = new Inicio();
        inicio.sincronizarTareas(usu.getCodigo(), getContext());

        Handler mHandler = new Handler();
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                listaTareas = new ArrayList<>();
                List<Tarea> tareasBD;
                tareasBD = db.obtenerTareasEstado(estado, usu.getCodigo());
                if (tareasBD != null) {
                    listaTareas.addAll(tareasBD);
                }
                if (listaTareas.size() > 0) {
                    refreshLayout.setVisibility(View.VISIBLE);
                    lin_vacio.setVisibility(View.GONE);
                } else {
                    refreshLayout.setVisibility(View.GONE);
                    lin_vacio.setVisibility(View.VISIBLE);
                }
                ((Inicio) getActivity()).actualizaContador();
                adp_tareas = new Adp_Tareas(listaTareas, getContext());
                adp_tareas.notifyDataSetChanged();
                recyclerView.setAdapter(adp_tareas);
                iniciarAnimacion(recyclerView);
            }
        },1000);
    }

    private void iniciarAnimacion(final RecyclerView recyclerView) {
        final Context context = recyclerView.getContext();
        final LayoutAnimationController controller = AnimationUtils.loadLayoutAnimation(context, R.anim.layout_animation_from_bottom);
        recyclerView.setLayoutAnimation(controller);
        recyclerView.getAdapter().notifyDataSetChanged();
        recyclerView.scheduleLayoutAnimation();
    }

    private BroadcastReceiver actualizar = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String filtro = MiFirebaseMessagingService.INFO_UPDATE_FILTER;
            if (intent.getAction().equals(filtro)) {
                cargaTareas(seleccion);
            }
        }
    };
}